import sys
import argparse
from gcode_outline import gcode_looper

parser = argparse.ArgumentParser()
parser.add_argument('filename',type=str,help='File to print')
parser.add_argument('height',type=int,help='Height in mm')
parser.add_argument('--base',action='store_true',help='Add a base.gcode (for things with holes)')
parser.add_argument('--reset_e',action='store_true',help='Reset E after breaks')
args = parser.parse_args()

looper = gcode_looper(args.reset_e)
looper.set_section('header', open('header.gcode'))
looper.set_section('footer', open('footer.gcode'))
if(args.base):
    looper.set_section('base', open('boring_base.gcode'))

program = open(args.filename).read()
outfile = open(args.filename + '.gcode', 'w')
looper.write(program, outfile, args.height)
