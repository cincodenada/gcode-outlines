class gcode_looper:
    def __init__(self, reset_e=False):
        self.sections = {
            'header': '',
            'footer': '',
            'base': '',
        }

        self.reset_e = reset_e

    def set_section(self, section, infile):
        self.sections[section] = infile.read()

    def start_subsection(self, outfile, reset_e = None):
        if(reset_e is None):
            reset_e = self.reset_e

        print("(New section)", file=outfile)
        if(reset_e):
            print("G92 E0", file=outfile)
            self.last_e = 0
        print("G1 F{:d}\nG1 E{:.3f}\nG1 F{:d}".format(1200, self.last_e-1, self.cur_speed), file=outfile)
        print("M103", file=outfile)
        self.next_start = True

    def write(self, layer_code, outfile, height):
        lines = layer_code.split("\n")
        # Trim trailing whitespace
        while(lines[-1].strip() == ""):
            lines.pop()

        if(self.sections['base']):
            curheight = 1.14
            self.cur_speed = 1000
        else:
            curheight = 0.14
            self.cur_speed = 1000

        self.start_extra = -0.1
        self.next_start = False
        self.last_e = 0
        self.last_empty = False

        print(self.sections['header'], file=outfile)
        print(self.sections['base'], file=outfile)
        for i in range(height):
            print("(Start layer)", file=outfile)
            print("M73 P{:.0f}".format(i/height), file=outfile)
            print("G1 Z{:.3f}".format(curheight), file=outfile)
            self.start_subsection(outfile, True)
            for line in lines:
                if(len(line)):
                    self.last_empty = False
                    if(line[0:3] == 'G1 '):
                        epos = line.find('E')
                        if(epos >= 0):
                            self.last_e = float(line[epos+1:])
                        print("{} F{}".format(line, self.cur_speed), file=outfile)
                    else:
                        print(line, file=outfile)

                    if(self.next_start):
                        print("G1 F{:d}\nG1 E{:.3f}\nG1 F{:d}".format(1200, self.last_e+self.start_extra, self.cur_speed), file=outfile)
                        print("G92 E{:0.3f}".format(self.last_e), file=outfile)
                        print("M101", file=outfile)
                        self.next_start = False
                elif not self.last_empty:
                    self.start_subsection(outfile)
                    self.last_empty = True

            curheight += 0.27
            self.cur_speed = 2100
        print(self.sections['footer'], file=outfile)
        outfile.close()
