(<format> skeinforge gcode </format>)
(<version> 12.03.14 </version>)
(<created> 16.05.19|8:41 </created>)
(<extruderInitialization>)
(<craftTypeName> extrusion </craftTypeName>)
M105
(<decimalPlacesCarried> 3 </decimalPlacesCarried>)
(<layerThickness> 0.27 </layerThickness>)
(<infillWidth> 0.4 </infillWidth>)
(<volumeFraction> 0.82 </volumeFraction>)
M73 P2 (display progress)
(<infillPerimeterOverlap> 0.3 </infillPerimeterOverlap>)
(<sharpestProduct> 0.866 </sharpestProduct>)
(<layerHeight> 0.27 </layerHeight>)
(<threadSequenceString> loops edge infill </threadSequenceString>)
(<maximumZFeedRatePerSecond> 999.0 </maximumZFeedRatePerSecond>)
(<objectFirstLayerFeedRateInfillMultiplier> 0.9 </objectFirstLayerFeedRateInfillMultiplier>)
(<operatingFeedRatePerSecond> 60.0 </operatingFeedRatePerSecond>)
(<objectFirstLayerFlowRateInfillMultiplier> 0.8 </objectFirstLayerFlowRateInfillMultiplier>)
(<operatingFlowRate> 60.0 </operatingFlowRate>)
(<orbitalFeedRatePerSecond> 0.0 </orbitalFeedRatePerSecond>)
(<travelFeedRatePerSecond> 75.0 </travelFeedRatePerSecond>)
(<edgeWidth> 0.4 </edgeWidth>)
(<perimeterWidth> 0.4 </perimeterWidth>)
(<profileName> ABS </profileName>)
(<settings>)
(<setting> alteration Activate_Alteration True </setting>)
(<setting> alteration Name_of_End_File:  </setting>)
(<setting> alteration Name_of_Start_File:  </setting>)
M73 P3 (display progress)
(<setting> alteration Remove_Redundant_Mcode True </setting>)
(<setting> alteration Replace_Variable_with_Setting True </setting>)
(<setting> comb Activate_Comb True </setting>)
(<setting> comb Running_Jump_Space_(mm): 2.0 </setting>)
(<setting> cool Activate_Cool True </setting>)
(<setting> cool Bridge_Cool_(Celcius): 1.0 </setting>)
(<setting> cool Orbit False </setting>)
(<setting> cool Slow_Down True </setting>)
(<setting> cool Maximum_Cool_(Celcius): 1.0 </setting>)
(<setting> cool Minimum_Layer_Time_(seconds): 5.0 </setting>)
(<setting> cool Minimum_Orbital_Radius_(millimeters): 10.0 </setting>)
(<setting> cool Name_of_Cool_End_File: cool_end.gcode </setting>)
(<setting> cool Name_of_Cool_Start_File: cool_start.gcode </setting>)
(<setting> cool Orbital_Outset_(millimeters): 2.0 </setting>)
(<setting> cool Turn_Fan_On_at_Beginning False </setting>)
(<setting> cool Turn_Fan_Off_at_Ending False </setting>)
(<setting> dimension Activate_Dimension True </setting>)
(<setting> dimension Absolute_Extrusion_Distance True </setting>)
M73 P4 (display progress)
(<setting> dimension Relative_Extrusion_Distance False </setting>)
(<setting> dimension Extruder_Retraction_Speed_(mm/s): 20.0 </setting>)
(<setting> dimension Filament_Diameter_(mm): 1.82 </setting>)
(<setting> dimension Filament_Packing_Density_(ratio): 0.85 </setting>)
(<setting> dimension Maximum_E_Value_before_Reset_(float): 91234.0 </setting>)
(<setting> dimension Minimum_Travel_for_Retraction_(millimeters): 1.0 </setting>)
(<setting> dimension Retract_Within_Island False </setting>)
(<setting> dimension Retraction_Distance_(millimeters): 1.0 </setting>)
(<setting> dimension Restart_Extra_Distance_(millimeters): 0.0 </setting>)
(<setting> export Activate_Export True </setting>)
(<setting> export Add_Descriptive_Extension False </setting>)
(<setting> export Add_Export_Suffix False </setting>)
(<setting> export Add_Profile_Extension False </setting>)
(<setting> export Add_Timestamp_Extension False </setting>)
(<setting> export Also_Send_Output_To:  </setting>)
(<setting> export Analyze_Gcode True </setting>)
(<setting> export Do_Not_Delete_Comments True </setting>)
(<setting> export Delete_Crafting_Comments False </setting>)
M73 P5 (display progress)
(<setting> export Delete_All_Comments False </setting>)
(<setting> export Do_Not_Change_Output True </setting>)
(<setting> export binary_16_byte False </setting>)
(<setting> export gcode_step False </setting>)
(<setting> export gcode_time_segment False </setting>)
(<setting> export gcode_small False </setting>)
(<setting> export File_Extension: gcode </setting>)
(<setting> export Name_of_Replace_File: replace.csv </setting>)
(<setting> export Save_Penultimate_Gcode False </setting>)
(<setting> fill Activate_Fill True </setting>)
(<setting> fill Diaphragm_Period_(layers): 100 </setting>)
(<setting> fill Diaphragm_Thickness_(layers): 0 </setting>)
(<setting> fill Extra_Shells_on_Alternating_Solid_Layer_(layers): 1 </setting>)
(<setting> fill Extra_Shells_on_Base_(layers): 1 </setting>)
(<setting> fill Extra_Shells_on_Sparse_Layer_(layers): 1 </setting>)
(<setting> fill Grid_Circle_Separation_over_Perimeter_Width_(ratio): 0.2 </setting>)
(<setting> fill Grid_Extra_Overlap_(ratio): 0.1 </setting>)
(<setting> fill Grid_Junction_Separation_Band_Height_(layers): 10 </setting>)
M73 P6 (display progress)
(<setting> fill Grid_Junction_Separation_over_Octogon_Radius_At_End_(ratio): 0.0 </setting>)
(<setting> fill Grid_Junction_Separation_over_Octogon_Radius_At_Middle_(ratio): 0.0 </setting>)
(<setting> fill Infill_Begin_Rotation_(degrees): 90.0 </setting>)
(<setting> fill Infill_Begin_Rotation_Repeat_(layers): 1 </setting>)
(<setting> fill Infill_Odd_Layer_Extra_Rotation_(degrees): 90.0 </setting>)
(<setting> fill Grid_Circular False </setting>)
(<setting> fill Grid_Hexagonal False </setting>)
(<setting> fill Grid_Rectangular True </setting>)
(<setting> fill Line False </setting>)
(<setting> fill Infill_Perimeter_Overlap_(ratio): 0.3 </setting>)
(<setting> fill Infill_Solidity_(ratio): 0.5 </setting>)
(<setting> fill Sharpest_Angle_(degrees): 60.0 </setting>)
(<setting> fill Solid_Surface_Thickness_(layers): 3 </setting>)
(<setting> fill Enable_automatic_solid_surface_thickness False </setting>)
(<setting> fill Desired_solid_surface_thickness_(mm): 0.75 </setting>)
(<setting> fill Off True </setting>)
(<setting> fill Force_odd_top_layer False </setting>)
(<setting> fill Force_even_top_layer False </setting>)
M73 P7 (display progress)
(<setting> fill Off True </setting>)
(<setting> fill Disable_top_layers False </setting>)
(<setting> fill Print_entire_object True </setting>)
(<setting> fill Only_print_1_layer False </setting>)
(<setting> fill Print_x_percentage_of_object False </setting>)
(<setting> fill Print_x_layers_of_object False </setting>)
(<setting> fill Percentage_of_object_to_print: 1.0 </setting>)
(<setting> fill Number_of_layers_to_print: 2.0 </setting>)
(<setting> fill Lower_Left True </setting>)
(<setting> fill Nearest False </setting>)
(<setting> fill Surrounding_Angle_(degrees): 60.0 </setting>)
(<setting> fill Infill_>_Loops_>_Perimeter False </setting>)
(<setting> fill Infill_>_Perimeter_>_Loops False </setting>)
(<setting> fill Loops_>_Infill_>_Perimeter False </setting>)
(<setting> fill Loops_>_Perimeter_>_Infill True </setting>)
(<setting> fill Perimeter_>_Infill_>_Loops False </setting>)
(<setting> fill Perimeter_>_Loops_>_Infill False </setting>)
(<setting> home Activate_Home True </setting>)
M73 P8 (display progress)
(<setting> home Name_of_Home_File: home.gcode </setting>)
(<setting> jitter Activate_Jitter True </setting>)
(<setting> jitter Jitter_Over_Perimeter_Width_(ratio): 2.0 </setting>)
(<setting> raft Activate_Raft True </setting>)
(<setting> raft Add_Raft,_Elevate_Nozzle,_Orbit: False </setting>)
(<setting> raft Base_Feed_Rate_Multiplier_(ratio): 0.75 </setting>)
(<setting> raft Base_Flow_Rate_Multiplier_(ratio): 0.6 </setting>)
(<setting> raft Base_Infill_Density_(ratio): 0.3 </setting>)
(<setting> raft Base_Layer_Thickness_over_Layer_Thickness: 2.2 </setting>)
(<setting> raft Base_Layers_(integer): 1 </setting>)
(<setting> raft Base_Nozzle_Lift_over_Base_Layer_Thickness_(ratio): 0.3 </setting>)
(<setting> raft Initial_Circling: False </setting>)
(<setting> raft Infill_Overhang_over_Extrusion_Width_(ratio): 0.1 </setting>)
(<setting> raft Interface_Feed_Rate_Multiplier_(ratio): 1.5 </setting>)
(<setting> raft Interface_Flow_Rate_Multiplier_(ratio): 0.7 </setting>)
(<setting> raft Interface_Infill_Density_(ratio): 0.4 </setting>)
(<setting> raft Interface_Layer_Thickness_over_Layer_Thickness: 1.8 </setting>)
M73 P9 (display progress)
(<setting> raft Interface_Layers_(integer): 2 </setting>)
(<setting> raft Interface_Nozzle_Lift_over_Interface_Layer_Thickness_(ratio): 0.3 </setting>)
(<setting> raft Name_of_Support_End_File: support_end.gcode </setting>)
(<setting> raft Name_of_Support_Start_File: support_start.gcode </setting>)
(<setting> raft Operating_Nozzle_Lift_over_Layer_Thickness_(ratio): 0.0 </setting>)
(<setting> raft Raft_Additional_Margin_over_Length_(%): 2.5 </setting>)
(<setting> raft Raft_Margin_(mm): 2.5 </setting>)
(<setting> raft Support_Cross_Hatch False </setting>)
(<setting> raft Support_Flow_Rate_over_Operating_Flow_Rate_(ratio): 0.7 </setting>)
(<setting> raft Support_Gap_over_Perimeter_Extrusion_Width_(ratio): 0.005 </setting>)
(<setting> raft None False </setting>)
(<setting> raft Empty_Layers_Only False </setting>)
(<setting> raft Everywhere False </setting>)
(<setting> raft Exterior_Only True </setting>)
(<setting> raft Support_Minimum_Angle_(degrees): 45.0 </setting>)
(<setting> speed Activate_Speed True </setting>)
(<setting> speed Add_Flow_Rate: True </setting>)
(<setting> speed Bridge_Feed_Rate_Multiplier_(ratio): 1.0 </setting>)
M73 P10 (display progress)
(<setting> speed Bridge_Flow_Rate_Multiplier_(ratio): 1.0 </setting>)
(<setting> speed Duty_Cyle_at_Beginning_(portion): 1.0 </setting>)
(<setting> speed Duty_Cyle_at_Ending_(portion): 0.0 </setting>)
(<setting> speed Feed_Rate_(mm/s): 60.0 </setting>)
(<setting> speed Flow_Rate_Setting_(float): 60.0 </setting>)
(<setting> speed Object_First_Layer_Feed_Rate_Infill_Multiplier_(ratio): 0.9 </setting>)
(<setting> speed Object_First_Layer_Feed_Rate_Perimeter_Multiplier_(ratio): 0.9 </setting>)
(<setting> speed Object_First_Layer_Flow_Rate_Infill_Multiplier_(ratio): 0.8 </setting>)
(<setting> speed Object_First_Layer_Flow_Rate_Perimeter_Multiplier_(ratio): 0.9 </setting>)
(<setting> speed Orbital_Feed_Rate_over_Operating_Feed_Rate_(ratio): 0.0 </setting>)
(<setting> speed Maximum_Z_Feed_Rate_(mm/s): 999.0 </setting>)
(<setting> speed Perimeter_Feed_Rate_Multiplier_(ratio): 0.5 </setting>)
(<setting> speed Perimeter_Flow_Rate_Multiplier_(ratio): 0.5 </setting>)
(<setting> speed Travel_Feed_Rate_(mm/s): 75.0 </setting>)
(</settings>)
(<timeStampPreface> 20160519_084126 </timeStampPreface>)
(<procedureName> carve </procedureName>)
(<procedureName> preface </procedureName>)
M73 P11 (display progress)
(<procedureName> inset </procedureName>)
(<procedureName> fill </procedureName>)
(<procedureName> speed </procedureName>)
(<procedureName> raft </procedureName>)
(<procedureName> jitter </procedureName>)
(<procedureName> comb </procedureName>)
(<procedureName> cool </procedureName>)
(<procedureName> dimension </procedureName>)
(<procedureName> alteration </procedureName>)
(<procedureName> export </procedureName>)
(</extruderInitialization>)
(<crafting>)
;M113 S1.0
;M108 S27.0
(<layer> 0.135 )
(<rotation> (6.12323399574e-17+1j) )
(<nestedRing>)
(<boundaryPerimeter>)
M73 P12 (display progress)
(<boundaryPoint> X35.0 Y-45.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X35.0 Y45.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y45.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y-45.0 Z0.135 </boundaryPoint>)
(<edge> outer )
G1 X-34.8 Y-44.79 Z0.14 F4500.0
G1 F1200.0
G1 E1.0
G1 F4500.0
M101
G1 X-34.8 Y-44.8 Z0.14 F1620.0 E1.0
G1 X34.8 Y-44.8 Z0.14 F1620.0 E4.4
G1 X34.8 Y44.8 Z0.14 F1620.0 E8.776
G1 X-34.8 Y44.8 Z0.14 F1620.0 E12.175
G1 X-34.8 Y-44.79 Z0.14 F1620.0 E16.551
G1 F1200.0
G1 E15.551
G1 F1620.0
M73 P13 (display progress)
M103
(</edge>)
(</boundaryPerimeter>)
(<nestedRing>)
(<boundaryPerimeter>)
(<boundaryPoint> X1.0 Y1.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X1.0 Y-1.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y-1.0 Z0.135 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y1.0 Z0.135 </boundaryPoint>)
(<edge> inner )
G1 X-1.19 Y-1.2 Z0.14 F4500.0
G1 F1200.0
G1 E16.551
G1 F4500.0
M101
G1 X-1.2 Y-1.2 Z0.14 F1620.0 E16.551
G1 X-1.2 Y1.2 Z0.14 F1620.0 E16.668
G1 X1.2 Y1.2 Z0.14 F1620.0 E16.785
M73 P14 (display progress)
G1 X1.2 Y-1.2 Z0.14 F1620.0 E16.902
G1 X-1.19 Y-1.2 Z0.14 F1620.0 E17.019
G1 F1200.0
G1 E17.019
G1 F1620.0
M103
(</edge>)
(</boundaryPerimeter>)
(</nestedRing>)
(<loop> inner )
;M108 S48.0
G1 X-1.19 Y-1.6 Z0.14 F4500.0
G1 F1200.0
G1 E17.019
G1 F4500.0
M101
G1 X-1.2 Y-1.6 Z0.14 F3240.0 E17.02
G1 X-1.6 Y-1.6 Z0.14 F3240.0 E17.037
M73 P15 (display progress)
G1 X-1.6 Y1.6 Z0.14 F3240.0 E17.176
G1 X1.6 Y1.6 Z0.14 F3240.0 E17.315
G1 X1.6 Y-1.6 Z0.14 F3240.0 E17.454
G1 X-1.19 Y-1.6 Z0.14 F3240.0 E17.575
G1 F1200.0
G1 E16.575
G1 F3240.0
M103
(</loop>)
(<loop> outer )
G1 X-34.4 Y-1.59 Z0.14 F4500.0
G1 F1200.0
G1 E17.575
G1 F4500.0
M101
G1 X-34.4 Y-1.6 Z0.14 F3240.0 E17.575
G1 X-34.4 Y-44.4 Z0.14 F3240.0 E19.433
G1 X34.4 Y-44.4 Z0.14 F3240.0 E22.42
M73 P16 (display progress)
G1 X34.4 Y44.4 Z0.14 F3240.0 E26.275
G1 X-34.4 Y44.4 Z0.14 F3240.0 E29.262
G1 X-34.4 Y-1.59 Z0.14 F3240.0 E31.259
G1 F1200.0
G1 E30.259
G1 F3240.0
M103
(</loop>)
(<infill>)
(<infillBoundary>)
(<infillPoint> X34.4 Y-44.4 Z0.135 </infillPoint>)
(<infillPoint> X34.4 Y44.4 Z0.135 </infillPoint>)
(<infillPoint> X-34.4 Y44.4 Z0.135 </infillPoint>)
(<infillPoint> X-34.4 Y-44.4 Z0.135 </infillPoint>)
(</infillBoundary>)
(<infillBoundary>)
(<infillPoint> X1.6 Y1.6 Z0.135 </infillPoint>)
M73 P17 (display progress)
(<infillPoint> X1.6 Y-1.6 Z0.135 </infillPoint>)
(<infillPoint> X-1.6 Y-1.6 Z0.135 </infillPoint>)
(<infillPoint> X-1.6 Y1.6 Z0.135 </infillPoint>)
(</infillBoundary>)
G1 X-1.6 Y-1.88 Z0.14 F4500.0
G1 F1200.0
G1 E31.259
G1 F4500.0
M101
G1 X-1.6 Y-44.12 Z0.14 F3240.0 E33.093
G1 X-1.2 Y-44.12 Z0.14 F3240.0 E33.11
G1 X-1.2 Y-1.88 Z0.14 F3240.0 E34.944
G1 X-0.8 Y-1.88 Z0.14 F3240.0 E34.961
G1 X-0.8 Y-44.12 Z0.14 F3240.0 E36.795
G1 X-0.4 Y-44.12 Z0.14 F3240.0 E36.812
G1 X-0.4 Y-1.88 Z0.14 F3240.0 E38.646
G1 X-0.0 Y-1.88 Z0.14 F3240.0 E38.663
G1 X-0.0 Y-44.12 Z0.14 F3240.0 E40.497
M73 P18 (display progress)
G1 X0.4 Y-44.12 Z0.14 F3240.0 E40.514
G1 X0.4 Y-1.88 Z0.14 F3240.0 E42.348
G1 X0.8 Y-1.88 Z0.14 F3240.0 E42.366
G1 X0.8 Y-44.12 Z0.14 F3240.0 E44.199
G1 X1.2 Y-44.12 Z0.14 F3240.0 E44.217
G1 X1.2 Y-1.88 Z0.14 F3240.0 E46.051
G1 X1.6 Y-1.88 Z0.14 F3240.0 E46.068
G1 X1.6 Y-44.12 Z0.14 F3240.0 E47.902
G1 X2.0 Y-44.12 Z0.14 F3240.0 E47.919
G1 X2.0 Y44.12 Z0.14 F3240.0 E51.75
G1 X2.4 Y44.12 Z0.14 F3240.0 E51.767
G1 X2.4 Y-44.12 Z0.14 F3240.0 E55.598
G1 X2.8 Y-44.12 Z0.14 F3240.0 E55.615
G1 X2.8 Y44.12 Z0.14 F3240.0 E59.446
G1 X3.2 Y44.12 Z0.14 F3240.0 E59.463
G1 X3.2 Y-44.12 Z0.14 F3240.0 E63.294
G1 X3.6 Y-44.12 Z0.14 F3240.0 E63.312
G1 X3.6 Y44.12 Z0.14 F3240.0 E67.142
M73 P19 (display progress)
G1 X4.0 Y44.12 Z0.14 F3240.0 E67.16
G1 X4.0 Y-44.12 Z0.14 F3240.0 E70.99
G1 X4.4 Y-44.12 Z0.14 F3240.0 E71.008
G1 X4.4 Y44.12 Z0.14 F3240.0 E74.839
G1 X4.8 Y44.12 Z0.14 F3240.0 E74.856
G1 X4.8 Y-44.12 Z0.14 F3240.0 E78.687
G1 X5.2 Y-44.12 Z0.14 F3240.0 E78.704
G1 X5.2 Y44.12 Z0.14 F3240.0 E82.535
G1 X5.6 Y44.12 Z0.14 F3240.0 E82.552
G1 X5.6 Y-44.12 Z0.14 F3240.0 E86.383
G1 X6.0 Y-44.12 Z0.14 F3240.0 E86.4
G1 X6.0 Y44.12 Z0.14 F3240.0 E90.231
G1 X6.4 Y44.12 Z0.14 F3240.0 E90.248
G1 X6.4 Y-44.12 Z0.14 F3240.0 E94.079
G1 X6.8 Y-44.12 Z0.14 F3240.0 E94.097
G1 X6.8 Y44.12 Z0.14 F3240.0 E97.927
G1 X7.2 Y44.12 Z0.14 F3240.0 E97.945
G1 X7.2 Y-44.12 Z0.14 F3240.0 E101.775
M73 P20 (display progress)
G1 X7.6 Y-44.12 Z0.14 F3240.0 E101.793
G1 X7.6 Y44.12 Z0.14 F3240.0 E105.624
G1 X8.0 Y44.12 Z0.14 F3240.0 E105.641
G1 X8.0 Y-44.12 Z0.14 F3240.0 E109.472
G1 X8.4 Y-44.12 Z0.14 F3240.0 E109.489
G1 X8.4 Y44.12 Z0.14 F3240.0 E113.32
G1 X8.8 Y44.12 Z0.14 F3240.0 E113.337
G1 X8.8 Y-44.12 Z0.14 F3240.0 E117.168
G1 X9.2 Y-44.12 Z0.14 F3240.0 E117.185
G1 X9.2 Y44.12 Z0.14 F3240.0 E121.016
G1 X9.6 Y44.12 Z0.14 F3240.0 E121.033
G1 X9.6 Y-44.12 Z0.14 F3240.0 E124.864
G1 X10.0 Y-44.12 Z0.14 F3240.0 E124.882
G1 X10.0 Y44.12 Z0.14 F3240.0 E128.712
G1 X10.4 Y44.12 Z0.14 F3240.0 E128.73
G1 X10.4 Y-44.12 Z0.14 F3240.0 E132.56
G1 X10.8 Y-44.12 Z0.14 F3240.0 E132.578
G1 X10.8 Y44.12 Z0.14 F3240.0 E136.409
M73 P21 (display progress)
G1 X11.2 Y44.12 Z0.14 F3240.0 E136.426
G1 X11.2 Y-44.12 Z0.14 F3240.0 E140.257
G1 X11.6 Y-44.12 Z0.14 F3240.0 E140.274
G1 X11.6 Y44.12 Z0.14 F3240.0 E144.105
G1 X12.0 Y44.12 Z0.14 F3240.0 E144.122
G1 X12.0 Y-44.12 Z0.14 F3240.0 E147.953
G1 X12.4 Y-44.12 Z0.14 F3240.0 E147.97
G1 X12.4 Y44.12 Z0.14 F3240.0 E151.801
G1 X12.8 Y44.12 Z0.14 F3240.0 E151.818
G1 X12.8 Y-44.12 Z0.14 F3240.0 E155.649
G1 X13.2 Y-44.12 Z0.14 F3240.0 E155.667
G1 X13.2 Y44.12 Z0.14 F3240.0 E159.497
G1 X13.6 Y44.12 Z0.14 F3240.0 E159.515
G1 X13.6 Y-44.12 Z0.14 F3240.0 E163.345
G1 X14.0 Y-44.12 Z0.14 F3240.0 E163.363
G1 X14.0 Y44.12 Z0.14 F3240.0 E167.194
G1 X14.4 Y44.12 Z0.14 F3240.0 E167.211
G1 X14.4 Y-44.12 Z0.14 F3240.0 E171.042
M73 P22 (display progress)
G1 X14.8 Y-44.12 Z0.14 F3240.0 E171.059
G1 X14.8 Y44.12 Z0.14 F3240.0 E174.89
G1 X15.2 Y44.12 Z0.14 F3240.0 E174.907
G1 X15.2 Y-44.12 Z0.14 F3240.0 E178.738
G1 X15.6 Y-44.12 Z0.14 F3240.0 E178.755
G1 X15.6 Y44.12 Z0.14 F3240.0 E182.586
G1 X16.0 Y44.12 Z0.14 F3240.0 E182.603
G1 X16.0 Y-44.12 Z0.14 F3240.0 E186.434
G1 X16.4 Y-44.12 Z0.14 F3240.0 E186.452
G1 X16.4 Y44.12 Z0.14 F3240.0 E190.282
G1 X16.8 Y44.12 Z0.14 F3240.0 E190.3
G1 X16.8 Y-44.12 Z0.14 F3240.0 E194.13
G1 X17.2 Y-44.12 Z0.14 F3240.0 E194.148
G1 X17.2 Y44.12 Z0.14 F3240.0 E197.979
G1 X17.6 Y44.12 Z0.14 F3240.0 E197.996
G1 X17.6 Y-44.12 Z0.14 F3240.0 E201.827
G1 X18.0 Y-44.12 Z0.14 F3240.0 E201.844
G1 X18.0 Y44.12 Z0.14 F3240.0 E205.675
M73 P23 (display progress)
G1 X18.4 Y44.12 Z0.14 F3240.0 E205.692
G1 X18.4 Y-44.12 Z0.14 F3240.0 E209.523
G1 X18.8 Y-44.12 Z0.14 F3240.0 E209.54
G1 X18.8 Y44.12 Z0.14 F3240.0 E213.371
G1 X19.2 Y44.12 Z0.14 F3240.0 E213.388
G1 X19.2 Y-44.12 Z0.14 F3240.0 E217.219
G1 X19.6 Y-44.12 Z0.14 F3240.0 E217.237
G1 X19.6 Y44.12 Z0.14 F3240.0 E221.067
G1 X20.0 Y44.12 Z0.14 F3240.0 E221.085
G1 X20.0 Y-44.12 Z0.14 F3240.0 E224.915
G1 X20.4 Y-44.12 Z0.14 F3240.0 E224.933
G1 X20.4 Y44.12 Z0.14 F3240.0 E228.764
G1 X20.8 Y44.12 Z0.14 F3240.0 E228.781
G1 X20.8 Y-44.12 Z0.14 F3240.0 E232.612
G1 X21.2 Y-44.12 Z0.14 F3240.0 E232.629
G1 X21.2 Y44.12 Z0.14 F3240.0 E236.46
G1 X21.6 Y44.12 Z0.14 F3240.0 E236.477
G1 X21.6 Y-44.12 Z0.14 F3240.0 E240.308
M73 P24 (display progress)
G1 X22.0 Y-44.12 Z0.14 F3240.0 E240.325
G1 X22.0 Y44.12 Z0.14 F3240.0 E244.156
G1 X22.4 Y44.12 Z0.14 F3240.0 E244.173
G1 X22.4 Y-44.12 Z0.14 F3240.0 E248.004
G1 X22.8 Y-44.12 Z0.14 F3240.0 E248.022
G1 X22.8 Y44.12 Z0.14 F3240.0 E251.852
G1 X23.2 Y44.12 Z0.14 F3240.0 E251.87
G1 X23.2 Y-44.12 Z0.14 F3240.0 E255.701
G1 X23.6 Y-44.12 Z0.14 F3240.0 E255.718
G1 X23.6 Y44.12 Z0.14 F3240.0 E259.549
G1 X24.0 Y44.12 Z0.14 F3240.0 E259.566
G1 X24.0 Y-44.12 Z0.14 F3240.0 E263.397
G1 X24.4 Y-44.12 Z0.14 F3240.0 E263.414
G1 X24.4 Y44.12 Z0.14 F3240.0 E267.245
G1 X24.8 Y44.12 Z0.14 F3240.0 E267.262
G1 X24.8 Y-44.12 Z0.14 F3240.0 E271.093
G1 X25.2 Y-44.12 Z0.14 F3240.0 E271.11
M73 P25 (display progress)
G1 X25.2 Y44.12 Z0.14 F3240.0 E274.941
G1 X25.6 Y44.12 Z0.14 F3240.0 E274.959
G1 X25.6 Y-44.12 Z0.14 F3240.0 E278.789
G1 X26.0 Y-44.12 Z0.14 F3240.0 E278.807
G1 X26.0 Y44.12 Z0.14 F3240.0 E282.637
G1 X26.4 Y44.12 Z0.14 F3240.0 E282.655
G1 X26.4 Y-44.12 Z0.14 F3240.0 E286.486
G1 X26.8 Y-44.12 Z0.14 F3240.0 E286.503
G1 X26.8 Y44.12 Z0.14 F3240.0 E290.334
G1 X27.2 Y44.12 Z0.14 F3240.0 E290.351
G1 X27.2 Y-44.12 Z0.14 F3240.0 E294.182
G1 X27.6 Y-44.12 Z0.14 F3240.0 E294.199
G1 X27.6 Y44.12 Z0.14 F3240.0 E298.03
G1 X28.0 Y44.12 Z0.14 F3240.0 E298.047
G1 X28.0 Y-44.12 Z0.14 F3240.0 E301.878
G1 X28.4 Y-44.12 Z0.14 F3240.0 E301.895
G1 X28.4 Y44.12 Z0.14 F3240.0 E305.726
G1 X28.8 Y44.12 Z0.14 F3240.0 E305.744
M73 P26 (display progress)
G1 X28.8 Y-44.12 Z0.14 F3240.0 E309.574
G1 X29.2 Y-44.12 Z0.14 F3240.0 E309.592
G1 X29.2 Y44.12 Z0.14 F3240.0 E313.422
G1 X29.6 Y44.12 Z0.14 F3240.0 E313.44
G1 X29.6 Y-44.12 Z0.14 F3240.0 E317.271
G1 X30.0 Y-44.12 Z0.14 F3240.0 E317.288
G1 X30.0 Y44.12 Z0.14 F3240.0 E321.119
G1 X30.4 Y44.12 Z0.14 F3240.0 E321.136
G1 X30.4 Y-44.12 Z0.14 F3240.0 E324.967
G1 X30.8 Y-44.12 Z0.14 F3240.0 E324.984
G1 X30.8 Y44.12 Z0.14 F3240.0 E328.815
G1 X31.2 Y44.12 Z0.14 F3240.0 E328.832
G1 X31.2 Y-44.12 Z0.14 F3240.0 E332.663
G1 X31.6 Y-44.12 Z0.14 F3240.0 E332.68
G1 X31.6 Y44.12 Z0.14 F3240.0 E336.511
G1 X32.0 Y44.12 Z0.14 F3240.0 E336.529
G1 X32.0 Y-44.12 Z0.14 F3240.0 E340.359
G1 X32.4 Y-44.12 Z0.14 F3240.0 E340.377
M73 P27 (display progress)
G1 X32.4 Y44.12 Z0.14 F3240.0 E344.207
G1 X32.8 Y44.12 Z0.14 F3240.0 E344.225
G1 X32.8 Y-44.12 Z0.14 F3240.0 E348.056
G1 X33.2 Y-44.12 Z0.14 F3240.0 E348.073
G1 X33.2 Y44.12 Z0.14 F3240.0 E351.904
G1 X33.6 Y44.12 Z0.14 F3240.0 E351.921
G1 X33.6 Y-44.12 Z0.14 F3240.0 E355.752
G1 X34.0 Y-44.01 Z0.14 F3240.0 E355.77
G1 X34.0 Y44.01 Z0.14 F3240.0 E359.591
G1 F1200.0
G1 E358.591
G1 F3240.0
M103
G1 X-1.6 Y1.88 Z0.14 F4500.0
G1 F1200.0
G1 E359.591
G1 F4500.0
M101
M73 P28 (display progress)
G1 X-1.6 Y44.12 Z0.14 F3240.0 E361.425
G1 X-1.2 Y44.12 Z0.14 F3240.0 E361.442
G1 X-1.2 Y1.88 Z0.14 F3240.0 E363.276
G1 X-0.8 Y1.88 Z0.14 F3240.0 E363.293
G1 X-0.8 Y44.12 Z0.14 F3240.0 E365.127
G1 X-0.4 Y44.12 Z0.14 F3240.0 E365.145
G1 X-0.4 Y1.88 Z0.14 F3240.0 E366.978
G1 X0.0 Y1.88 Z0.14 F3240.0 E366.996
G1 X0.0 Y44.12 Z0.14 F3240.0 E368.829
G1 X0.4 Y44.12 Z0.14 F3240.0 E368.847
G1 X0.4 Y1.88 Z0.14 F3240.0 E370.681
G1 X0.8 Y1.88 Z0.14 F3240.0 E370.698
G1 X0.8 Y44.12 Z0.14 F3240.0 E372.532
G1 X1.2 Y44.12 Z0.14 F3240.0 E372.549
G1 X1.2 Y1.88 Z0.14 F3240.0 E374.383
G1 X1.6 Y1.88 Z0.14 F3240.0 E374.4
G1 X1.6 Y44.12 Z0.14 F3240.0 E376.234
G1 F1200.0
M73 P29 (display progress)
G1 E375.234
G1 F3240.0
M103
G1 X-2.0 Y44.12 Z0.14 F4500.0
G1 F1200.0
G1 E376.234
G1 F4500.0
M101
G1 X-2.0 Y-44.12 Z0.14 F3240.0 E380.065
G1 X-2.4 Y-44.12 Z0.14 F3240.0 E380.082
G1 X-2.4 Y44.12 Z0.14 F3240.0 E383.913
G1 X-2.8 Y44.12 Z0.14 F3240.0 E383.93
G1 X-2.8 Y-44.12 Z0.14 F3240.0 E387.761
G1 X-3.2 Y-44.12 Z0.14 F3240.0 E387.778
G1 X-3.2 Y44.12 Z0.14 F3240.0 E391.609
G1 X-3.6 Y44.12 Z0.14 F3240.0 E391.626
G1 X-3.6 Y-44.12 Z0.14 F3240.0 E395.457
G1 X-4.0 Y-44.12 Z0.14 F3240.0 E395.475
M73 P30 (display progress)
G1 X-4.0 Y44.12 Z0.14 F3240.0 E399.305
G1 X-4.4 Y44.12 Z0.14 F3240.0 E399.323
G1 X-4.4 Y-44.12 Z0.14 F3240.0 E403.154
G1 X-4.8 Y-44.12 Z0.14 F3240.0 E403.171
G1 X-4.8 Y44.12 Z0.14 F3240.0 E407.002
G1 X-5.2 Y44.12 Z0.14 F3240.0 E407.019
G1 X-5.2 Y-44.12 Z0.14 F3240.0 E410.85
G1 X-5.6 Y-44.12 Z0.14 F3240.0 E410.867
G1 X-5.6 Y44.12 Z0.14 F3240.0 E414.698
G1 X-6.0 Y44.12 Z0.14 F3240.0 E414.715
G1 X-6.0 Y-44.12 Z0.14 F3240.0 E418.546
G1 X-6.4 Y-44.12 Z0.14 F3240.0 E418.563
G1 X-6.4 Y44.12 Z0.14 F3240.0 E422.394
G1 X-6.8 Y44.12 Z0.14 F3240.0 E422.412
G1 X-6.8 Y-44.12 Z0.14 F3240.0 E426.242
G1 X-7.2 Y-44.12 Z0.14 F3240.0 E426.26
G1 X-7.2 Y44.12 Z0.14 F3240.0 E430.09
G1 X-7.6 Y44.12 Z0.14 F3240.0 E430.108
M73 P31 (display progress)
G1 X-7.6 Y-44.12 Z0.14 F3240.0 E433.939
G1 X-8.0 Y-44.12 Z0.14 F3240.0 E433.956
G1 X-8.0 Y44.12 Z0.14 F3240.0 E437.787
G1 X-8.4 Y44.12 Z0.14 F3240.0 E437.804
G1 X-8.4 Y-44.12 Z0.14 F3240.0 E441.635
G1 X-8.8 Y-44.12 Z0.14 F3240.0 E441.652
G1 X-8.8 Y44.12 Z0.14 F3240.0 E445.483
G1 X-9.2 Y44.12 Z0.14 F3240.0 E445.5
G1 X-9.2 Y-44.12 Z0.14 F3240.0 E449.331
G1 X-9.6 Y-44.12 Z0.14 F3240.0 E449.348
G1 X-9.6 Y44.12 Z0.14 F3240.0 E453.179
G1 X-10.0 Y44.12 Z0.14 F3240.0 E453.197
G1 X-10.0 Y-44.12 Z0.14 F3240.0 E457.027
G1 X-10.4 Y-44.12 Z0.14 F3240.0 E457.045
G1 X-10.4 Y44.12 Z0.14 F3240.0 E460.875
G1 X-10.8 Y44.12 Z0.14 F3240.0 E460.893
G1 X-10.8 Y-44.12 Z0.14 F3240.0 E464.724
G1 X-11.2 Y-44.12 Z0.14 F3240.0 E464.741
M73 P32 (display progress)
G1 X-11.2 Y44.12 Z0.14 F3240.0 E468.572
G1 X-11.6 Y44.12 Z0.14 F3240.0 E468.589
G1 X-11.6 Y-44.12 Z0.14 F3240.0 E472.42
G1 X-12.0 Y-44.12 Z0.14 F3240.0 E472.437
G1 X-12.0 Y44.12 Z0.14 F3240.0 E476.268
G1 X-12.4 Y44.12 Z0.14 F3240.0 E476.285
G1 X-12.4 Y-44.12 Z0.14 F3240.0 E480.116
G1 X-12.8 Y-44.12 Z0.14 F3240.0 E480.133
G1 X-12.8 Y44.12 Z0.14 F3240.0 E483.964
G1 X-13.2 Y44.12 Z0.14 F3240.0 E483.982
G1 X-13.2 Y-44.12 Z0.14 F3240.0 E487.812
G1 X-13.6 Y-44.12 Z0.14 F3240.0 E487.83
G1 X-13.6 Y44.12 Z0.14 F3240.0 E491.66
G1 X-14.0 Y44.12 Z0.14 F3240.0 E491.678
G1 X-14.0 Y-44.12 Z0.14 F3240.0 E495.509
G1 X-14.4 Y-44.12 Z0.14 F3240.0 E495.526
G1 X-14.4 Y44.12 Z0.14 F3240.0 E499.357
G1 X-14.8 Y44.12 Z0.14 F3240.0 E499.374
M73 P33 (display progress)
G1 X-14.8 Y-44.12 Z0.14 F3240.0 E503.205
G1 X-15.2 Y-44.12 Z0.14 F3240.0 E503.222
G1 X-15.2 Y44.12 Z0.14 F3240.0 E507.053
G1 X-15.6 Y44.12 Z0.14 F3240.0 E507.07
G1 X-15.6 Y-44.12 Z0.14 F3240.0 E510.901
G1 X-16.0 Y-44.12 Z0.14 F3240.0 E510.918
G1 X-16.0 Y44.12 Z0.14 F3240.0 E514.749
G1 X-16.4 Y44.12 Z0.14 F3240.0 E514.767
G1 X-16.4 Y-44.12 Z0.14 F3240.0 E518.597
G1 X-16.8 Y-44.12 Z0.14 F3240.0 E518.615
G1 X-16.8 Y44.12 Z0.14 F3240.0 E522.445
G1 X-17.2 Y44.12 Z0.14 F3240.0 E522.463
G1 X-17.2 Y-44.12 Z0.14 F3240.0 E526.294
G1 X-17.6 Y-44.12 Z0.14 F3240.0 E526.311
G1 X-17.6 Y44.12 Z0.14 F3240.0 E530.142
G1 X-18.0 Y44.12 Z0.14 F3240.0 E530.159
G1 X-18.0 Y-44.12 Z0.14 F3240.0 E533.99
M73 P34 (display progress)
G1 X-18.4 Y-44.12 Z0.14 F3240.0 E534.007
G1 X-18.4 Y44.12 Z0.14 F3240.0 E537.838
G1 X-18.8 Y44.12 Z0.14 F3240.0 E537.855
G1 X-18.8 Y-44.12 Z0.14 F3240.0 E541.686
G1 X-19.2 Y-44.12 Z0.14 F3240.0 E541.703
G1 X-19.2 Y44.12 Z0.14 F3240.0 E545.534
G1 X-19.6 Y44.12 Z0.14 F3240.0 E545.552
G1 X-19.6 Y-44.12 Z0.14 F3240.0 E549.382
G1 X-20.0 Y-44.12 Z0.14 F3240.0 E549.4
G1 X-20.0 Y44.12 Z0.14 F3240.0 E553.23
G1 X-20.4 Y44.12 Z0.14 F3240.0 E553.248
G1 X-20.4 Y-44.12 Z0.14 F3240.0 E557.079
G1 X-20.8 Y-44.12 Z0.14 F3240.0 E557.096
G1 X-20.8 Y44.12 Z0.14 F3240.0 E560.927
G1 X-21.2 Y44.12 Z0.14 F3240.0 E560.944
G1 X-21.2 Y-44.12 Z0.14 F3240.0 E564.775
G1 X-21.6 Y-44.12 Z0.14 F3240.0 E564.792
G1 X-21.6 Y44.12 Z0.14 F3240.0 E568.623
M73 P35 (display progress)
G1 X-22.0 Y44.12 Z0.14 F3240.0 E568.64
G1 X-22.0 Y-44.12 Z0.14 F3240.0 E572.471
G1 X-22.4 Y-44.12 Z0.14 F3240.0 E572.488
G1 X-22.4 Y44.12 Z0.14 F3240.0 E576.319
G1 X-22.8 Y44.12 Z0.14 F3240.0 E576.337
G1 X-22.8 Y-44.12 Z0.14 F3240.0 E580.167
G1 X-23.2 Y-44.12 Z0.14 F3240.0 E580.185
G1 X-23.2 Y44.12 Z0.14 F3240.0 E584.015
G1 X-23.6 Y44.12 Z0.14 F3240.0 E584.033
G1 X-23.6 Y-44.12 Z0.14 F3240.0 E587.864
G1 X-24.0 Y-44.12 Z0.14 F3240.0 E587.881
G1 X-24.0 Y44.12 Z0.14 F3240.0 E591.712
G1 X-24.4 Y44.12 Z0.14 F3240.0 E591.729
G1 X-24.4 Y-44.12 Z0.14 F3240.0 E595.56
G1 X-24.8 Y-44.12 Z0.14 F3240.0 E595.577
G1 X-24.8 Y44.12 Z0.14 F3240.0 E599.408
G1 X-25.2 Y44.12 Z0.14 F3240.0 E599.425
G1 X-25.2 Y-44.12 Z0.14 F3240.0 E603.256
M73 P36 (display progress)
G1 X-25.6 Y-44.12 Z0.14 F3240.0 E603.273
G1 X-25.6 Y44.12 Z0.14 F3240.0 E607.104
G1 X-26.0 Y44.12 Z0.14 F3240.0 E607.122
G1 X-26.0 Y-44.12 Z0.14 F3240.0 E610.952
G1 X-26.4 Y-44.12 Z0.14 F3240.0 E610.97
G1 X-26.4 Y44.12 Z0.14 F3240.0 E614.8
G1 X-26.8 Y44.12 Z0.14 F3240.0 E614.818
G1 X-26.8 Y-44.12 Z0.14 F3240.0 E618.649
G1 X-27.2 Y-44.12 Z0.14 F3240.0 E618.666
G1 X-27.2 Y44.12 Z0.14 F3240.0 E622.497
G1 X-27.6 Y44.12 Z0.14 F3240.0 E622.514
G1 X-27.6 Y-44.12 Z0.14 F3240.0 E626.345
G1 X-28.0 Y-44.12 Z0.14 F3240.0 E626.362
G1 X-28.0 Y44.12 Z0.14 F3240.0 E630.193
G1 X-28.4 Y44.12 Z0.14 F3240.0 E630.21
G1 X-28.4 Y-44.12 Z0.14 F3240.0 E634.041
G1 X-28.8 Y-44.12 Z0.14 F3240.0 E634.058
G1 X-28.8 Y44.12 Z0.14 F3240.0 E637.889
M73 P37 (display progress)
G1 X-29.2 Y44.12 Z0.14 F3240.0 E637.907
G1 X-29.2 Y-44.12 Z0.14 F3240.0 E641.737
G1 X-29.6 Y-44.12 Z0.14 F3240.0 E641.755
G1 X-29.6 Y44.12 Z0.14 F3240.0 E645.586
G1 X-30.0 Y44.12 Z0.14 F3240.0 E645.603
G1 X-30.0 Y-44.12 Z0.14 F3240.0 E649.434
G1 X-30.4 Y-44.12 Z0.14 F3240.0 E649.451
G1 X-30.4 Y44.12 Z0.14 F3240.0 E653.282
G1 X-30.8 Y44.12 Z0.14 F3240.0 E653.299
G1 X-30.8 Y-44.12 Z0.14 F3240.0 E657.13
G1 X-31.2 Y-44.12 Z0.14 F3240.0 E657.147
G1 X-31.2 Y44.12 Z0.14 F3240.0 E660.978
G1 X-31.6 Y44.12 Z0.14 F3240.0 E660.995
G1 X-31.6 Y-44.12 Z0.14 F3240.0 E664.826
G1 X-32.0 Y-44.12 Z0.14 F3240.0 E664.844
G1 X-32.0 Y44.12 Z0.14 F3240.0 E668.674
G1 X-32.4 Y44.12 Z0.14 F3240.0 E668.692
G1 X-32.4 Y-44.12 Z0.14 F3240.0 E672.522
M73 P38 (display progress)
G1 X-32.8 Y-44.12 Z0.14 F3240.0 E672.54
G1 X-32.8 Y44.12 Z0.14 F3240.0 E676.371
G1 X-33.2 Y44.12 Z0.14 F3240.0 E676.388
G1 X-33.2 Y-44.12 Z0.14 F3240.0 E680.219
G1 X-33.6 Y-44.12 Z0.14 F3240.0 E680.236
G1 X-33.6 Y44.12 Z0.14 F3240.0 E684.067
G1 X-34.0 Y44.01 Z0.14 F3240.0 E684.085
G1 X-34.0 Y-44.01 Z0.14 F3240.0 E687.906
G1 F1200.0
G1 E686.906
G1 F3240.0
M103
(</infill>)
(</nestedRing>)
(</rotation>)
(</layer>)
;M108 S60.0
(<layer> 0.405 )
M73 P39 (display progress)
(<rotation> (-1+1.22464679915e-16j) )
(<nestedRing>)
(<boundaryPerimeter>)
(<boundaryPoint> X35.0 Y-45.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X35.0 Y45.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y45.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y-45.0 Z0.405 </boundaryPoint>)
(<loop> inner )
G1 X-1.29 Y-1.6 Z0.41 F4500.0
G1 F1200.0
G1 E687.906
G1 F4500.0
M101
G1 X-1.6 Y-1.6 Z0.41 F3600.0 E687.921
G1 X-1.6 Y1.6 Z0.41 F3600.0 E688.078
G1 X1.6 Y1.6 Z0.41 F3600.0 E688.234
G1 X1.6 Y-1.6 Z0.41 F3600.0 E688.39
G1 X-1.29 Y-1.6 Z0.41 F3600.0 E688.531
M73 P40 (display progress)
G1 F1200.0
G1 E687.531
G1 F3600.0
M103
(</loop>)
(<loop> outer )
G1 X-34.4 Y-1.29 Z0.41 F4500.0
G1 F1200.0
G1 E688.531
G1 F4500.0
M101
G1 X-34.4 Y-1.6 Z0.41 F3600.0 E688.546
G1 X-34.4 Y-44.4 Z0.41 F3600.0 E690.637
G1 X34.4 Y-44.4 Z0.41 F3600.0 E693.997
G1 X34.4 Y44.4 Z0.41 F3600.0 E698.334
G1 X-34.4 Y44.4 Z0.41 F3600.0 E701.694
G1 X-34.4 Y-1.29 Z0.41 F3600.0 E703.926
G1 F1200.0
M73 P41 (display progress)
G1 E703.926
G1 F3600.0
M103
(</loop>)
(<edge> outer )
;M108 S30.0
G1 X-34.8 Y-1.29 Z0.41 F4500.0
G1 F1200.0
G1 E703.926
G1 F4500.0
M101
G1 X-34.8 Y-1.6 Z0.41 F1800.0 E703.941
G1 X-34.8 Y-44.8 Z0.41 F1800.0 E706.051
G1 X34.8 Y-44.8 Z0.41 F1800.0 E709.45
G1 X34.8 Y44.8 Z0.41 F1800.0 E713.826
G1 X-34.8 Y44.8 Z0.41 F1800.0 E717.225
G1 X-34.8 Y-1.29 Z0.41 F1800.0 E719.476
M73 P42 (display progress)
G1 F1200.0
G1 E718.476
G1 F1800.0
M103
(</edge>)
(</boundaryPerimeter>)
(<nestedRing>)
(<boundaryPerimeter>)
(<boundaryPoint> X1.0 Y1.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X1.0 Y-1.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y-1.0 Z0.405 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y1.0 Z0.405 </boundaryPoint>)
(<edge> inner )
G1 X-0.89 Y-1.2 Z0.41 F4500.0
G1 F1200.0
G1 E719.476
G1 F4500.0
M101
M73 P43 (display progress)
G1 X-1.2 Y-1.2 Z0.41 F1800.0 E719.491
G1 X-1.2 Y1.2 Z0.41 F1800.0 E719.608
G1 X1.2 Y1.2 Z0.41 F1800.0 E719.726
G1 X1.2 Y-1.2 Z0.41 F1800.0 E719.843
G1 X-0.89 Y-1.2 Z0.41 F1800.0 E719.945
G1 F1200.0
G1 E718.945
G1 F1800.0
M103
(</edge>)
(</boundaryPerimeter>)
(</nestedRing>)
(<infill>)
(<infillBoundary>)
(<infillPoint> X34.4 Y-44.4 Z0.405 </infillPoint>)
(<infillPoint> X34.4 Y44.4 Z0.405 </infillPoint>)
(<infillPoint> X-34.4 Y44.4 Z0.405 </infillPoint>)
(<infillPoint> X-34.4 Y-44.4 Z0.405 </infillPoint>)
M73 P44 (display progress)
(</infillBoundary>)
(<infillBoundary>)
(<infillPoint> X1.6 Y1.6 Z0.405 </infillPoint>)
(<infillPoint> X1.6 Y-1.6 Z0.405 </infillPoint>)
(<infillPoint> X-1.6 Y-1.6 Z0.405 </infillPoint>)
(<infillPoint> X-1.6 Y1.6 Z0.405 </infillPoint>)
(</infillBoundary>)
;M108 S60.0
G1 X-0.96 Y-1.4 Z0.41 F4500.0
G1 X-1.88 Y1.6 Z0.41 F4500.0
G1 F1200.0
G1 E719.945
G1 F4500.0
M101
G1 X-34.12 Y1.6 Z0.41 F3600.0 E721.52
G1 X-34.12 Y1.2 Z0.41 F3600.0 E721.539
G1 X-1.88 Y1.2 Z0.41 F3600.0 E723.114
G1 X-1.88 Y0.8 Z0.41 F3600.0 E723.133
M73 P45 (display progress)
G1 X-34.12 Y0.8 Z0.41 F3600.0 E724.708
G1 X-34.12 Y0.4 Z0.41 F3600.0 E724.727
G1 X-1.88 Y0.4 Z0.41 F3600.0 E726.302
G1 X-1.88 Y0.0 Z0.41 F3600.0 E726.321
G1 X-34.12 Y0.0 Z0.41 F3600.0 E727.896
G1 X-34.12 Y-0.4 Z0.41 F3600.0 E727.916
G1 X-1.88 Y-0.4 Z0.41 F3600.0 E729.49
G1 X-1.88 Y-0.8 Z0.41 F3600.0 E729.51
G1 X-34.12 Y-0.8 Z0.41 F3600.0 E731.084
G1 X-34.12 Y-1.2 Z0.41 F3600.0 E731.104
G1 X-1.88 Y-1.2 Z0.41 F3600.0 E732.678
G1 X-1.88 Y-1.6 Z0.41 F3600.0 E732.698
G1 X-34.12 Y-1.6 Z0.41 F3600.0 E734.273
G1 F1200.0
G1 E733.273
G1 F3600.0
M103
G1 X-34.12 Y2.0 Z0.41 F4500.0
M73 P46 (display progress)
G1 F1200.0
G1 E734.273
G1 F4500.0
M101
G1 X34.12 Y2.0 Z0.41 F3600.0 E737.605
G1 X34.12 Y2.4 Z0.41 F3600.0 E737.625
G1 X-34.12 Y2.4 Z0.41 F3600.0 E740.958
G1 X-34.12 Y2.8 Z0.41 F3600.0 E740.977
G1 X34.12 Y2.8 Z0.41 F3600.0 E744.31
G1 X34.12 Y3.2 Z0.41 F3600.0 E744.33
G1 X-34.12 Y3.2 Z0.41 F3600.0 E747.662
G1 X-34.12 Y3.6 Z0.41 F3600.0 E747.682
G1 X34.12 Y3.6 Z0.41 F3600.0 E751.015
G1 X34.12 Y4.0 Z0.41 F3600.0 E751.034
G1 X-34.12 Y4.0 Z0.41 F3600.0 E754.367
G1 X-34.12 Y4.4 Z0.41 F3600.0 E754.387
G1 X34.12 Y4.4 Z0.41 F3600.0 E757.719
G1 X34.12 Y4.8 Z0.41 F3600.0 E757.739
M73 P47 (display progress)
G1 X-34.12 Y4.8 Z0.41 F3600.0 E761.072
G1 X-34.12 Y5.2 Z0.41 F3600.0 E761.091
G1 X34.12 Y5.2 Z0.41 F3600.0 E764.424
G1 X34.12 Y5.6 Z0.41 F3600.0 E764.444
G1 X-34.12 Y5.6 Z0.41 F3600.0 E767.776
G1 X-34.12 Y6.0 Z0.41 F3600.0 E767.796
G1 X34.12 Y6.0 Z0.41 F3600.0 E771.129
G1 X34.12 Y6.4 Z0.41 F3600.0 E771.148
G1 X-34.12 Y6.4 Z0.41 F3600.0 E774.481
G1 X-34.12 Y6.8 Z0.41 F3600.0 E774.501
G1 X34.12 Y6.8 Z0.41 F3600.0 E777.834
G1 X34.12 Y7.2 Z0.41 F3600.0 E777.853
G1 X-34.12 Y7.2 Z0.41 F3600.0 E781.186
G1 X-34.12 Y7.6 Z0.41 F3600.0 E781.205
G1 X34.12 Y7.6 Z0.41 F3600.0 E784.538
G1 X34.12 Y8.0 Z0.41 F3600.0 E784.558
G1 X-34.12 Y8.0 Z0.41 F3600.0 E787.891
G1 X-34.12 Y8.4 Z0.41 F3600.0 E787.91
M73 P48 (display progress)
G1 X34.12 Y8.4 Z0.41 F3600.0 E791.243
G1 X34.12 Y8.8 Z0.41 F3600.0 E791.262
G1 X-34.12 Y8.8 Z0.41 F3600.0 E794.595
G1 X-34.12 Y9.2 Z0.41 F3600.0 E794.615
G1 X34.12 Y9.2 Z0.41 F3600.0 E797.948
G1 X34.12 Y9.6 Z0.41 F3600.0 E797.967
G1 X-34.12 Y9.6 Z0.41 F3600.0 E801.3
G1 X-34.12 Y10.0 Z0.41 F3600.0 E801.32
G1 X34.12 Y10.0 Z0.41 F3600.0 E804.652
G1 X34.12 Y10.4 Z0.41 F3600.0 E804.672
G1 X-34.12 Y10.4 Z0.41 F3600.0 E808.005
G1 X-34.12 Y10.8 Z0.41 F3600.0 E808.024
G1 X34.12 Y10.8 Z0.41 F3600.0 E811.357
G1 X34.12 Y11.2 Z0.41 F3600.0 E811.377
G1 X-34.12 Y11.2 Z0.41 F3600.0 E814.709
G1 X-34.12 Y11.6 Z0.41 F3600.0 E814.729
G1 X34.12 Y11.6 Z0.41 F3600.0 E818.062
G1 X34.12 Y12.0 Z0.41 F3600.0 E818.081
M73 P49 (display progress)
G1 X-34.12 Y12.0 Z0.41 F3600.0 E821.414
G1 X-34.12 Y12.4 Z0.41 F3600.0 E821.434
G1 X34.12 Y12.4 Z0.41 F3600.0 E824.766
G1 X34.12 Y12.8 Z0.41 F3600.0 E824.786
G1 X-34.12 Y12.8 Z0.41 F3600.0 E828.119
G1 X-34.12 Y13.2 Z0.41 F3600.0 E828.138
G1 X34.12 Y13.2 Z0.41 F3600.0 E831.471
G1 X34.12 Y13.6 Z0.41 F3600.0 E831.491
G1 X-34.12 Y13.6 Z0.41 F3600.0 E834.824
G1 X-34.12 Y14.0 Z0.41 F3600.0 E834.843
G1 X34.12 Y14.0 Z0.41 F3600.0 E838.176
G1 X34.12 Y14.4 Z0.41 F3600.0 E838.195
G1 X-34.12 Y14.4 Z0.41 F3600.0 E841.528
G1 X-34.12 Y14.8 Z0.41 F3600.0 E841.548
G1 X34.12 Y14.8 Z0.41 F3600.0 E844.881
G1 X34.12 Y15.2 Z0.41 F3600.0 E844.9
G1 X-34.12 Y15.2 Z0.41 F3600.0 E848.233
M73 P50 (display progress)
G1 X-34.12 Y15.6 Z0.41 F3600.0 E848.252
G1 X34.12 Y15.6 Z0.41 F3600.0 E851.585
G1 X34.12 Y16.0 Z0.41 F3600.0 E851.605
G1 X-34.12 Y16.0 Z0.41 F3600.0 E854.938
G1 X-34.12 Y16.4 Z0.41 F3600.0 E854.957
G1 X34.12 Y16.4 Z0.41 F3600.0 E858.29
G1 X34.12 Y16.8 Z0.41 F3600.0 E858.31
G1 X-34.12 Y16.8 Z0.41 F3600.0 E861.642
G1 X-34.12 Y17.2 Z0.41 F3600.0 E861.662
G1 X34.12 Y17.2 Z0.41 F3600.0 E864.995
G1 X34.12 Y17.6 Z0.41 F3600.0 E865.014
G1 X-34.12 Y17.6 Z0.41 F3600.0 E868.347
G1 X-34.12 Y18.0 Z0.41 F3600.0 E868.367
G1 X34.12 Y18.0 Z0.41 F3600.0 E871.699
G1 X34.12 Y18.4 Z0.41 F3600.0 E871.719
G1 X-34.12 Y18.4 Z0.41 F3600.0 E875.052
G1 X-34.12 Y18.8 Z0.41 F3600.0 E875.071
G1 X34.12 Y18.8 Z0.41 F3600.0 E878.404
M73 P51 (display progress)
G1 X34.12 Y19.2 Z0.41 F3600.0 E878.424
G1 X-34.12 Y19.2 Z0.41 F3600.0 E881.756
G1 X-34.12 Y19.6 Z0.41 F3600.0 E881.776
G1 X34.12 Y19.6 Z0.41 F3600.0 E885.109
G1 X34.12 Y20.0 Z0.41 F3600.0 E885.128
G1 X-34.12 Y20.0 Z0.41 F3600.0 E888.461
G1 X-34.12 Y20.4 Z0.41 F3600.0 E888.481
G1 X34.12 Y20.4 Z0.41 F3600.0 E891.813
G1 X34.12 Y20.8 Z0.41 F3600.0 E891.833
G1 X-34.12 Y20.8 Z0.41 F3600.0 E895.166
G1 X-34.12 Y21.2 Z0.41 F3600.0 E895.185
G1 X34.12 Y21.2 Z0.41 F3600.0 E898.518
G1 X34.12 Y21.6 Z0.41 F3600.0 E898.538
G1 X-34.12 Y21.6 Z0.41 F3600.0 E901.871
G1 X-34.12 Y22.0 Z0.41 F3600.0 E901.89
G1 X34.12 Y22.0 Z0.41 F3600.0 E905.223
G1 X34.12 Y22.4 Z0.41 F3600.0 E905.242
G1 X-34.12 Y22.4 Z0.41 F3600.0 E908.575
M73 P52 (display progress)
G1 X-34.12 Y22.8 Z0.41 F3600.0 E908.595
G1 X34.12 Y22.8 Z0.41 F3600.0 E911.928
G1 X34.12 Y23.2 Z0.41 F3600.0 E911.947
G1 X-34.12 Y23.2 Z0.41 F3600.0 E915.28
G1 X-34.12 Y23.6 Z0.41 F3600.0 E915.299
G1 X34.12 Y23.6 Z0.41 F3600.0 E918.632
G1 X34.12 Y24.0 Z0.41 F3600.0 E918.652
G1 X-34.12 Y24.0 Z0.41 F3600.0 E921.985
G1 X-34.12 Y24.4 Z0.41 F3600.0 E922.004
G1 X34.12 Y24.4 Z0.41 F3600.0 E925.337
G1 X34.12 Y24.8 Z0.41 F3600.0 E925.357
G1 X-34.12 Y24.8 Z0.41 F3600.0 E928.689
G1 X-34.12 Y25.2 Z0.41 F3600.0 E928.709
G1 X34.12 Y25.2 Z0.41 F3600.0 E932.042
G1 X34.12 Y25.6 Z0.41 F3600.0 E932.061
G1 X-34.12 Y25.6 Z0.41 F3600.0 E935.394
G1 X-34.12 Y26.0 Z0.41 F3600.0 E935.414
G1 X34.12 Y26.0 Z0.41 F3600.0 E938.746
M73 P53 (display progress)
G1 X34.12 Y26.4 Z0.41 F3600.0 E938.766
G1 X-34.12 Y26.4 Z0.41 F3600.0 E942.099
G1 X-34.12 Y26.8 Z0.41 F3600.0 E942.118
G1 X34.12 Y26.8 Z0.41 F3600.0 E945.451
G1 X34.12 Y27.2 Z0.41 F3600.0 E945.471
G1 X-34.12 Y27.2 Z0.41 F3600.0 E948.803
G1 X-34.12 Y27.6 Z0.41 F3600.0 E948.823
G1 X34.12 Y27.6 Z0.41 F3600.0 E952.156
G1 X34.12 Y28.0 Z0.41 F3600.0 E952.175
G1 X-34.12 Y28.0 Z0.41 F3600.0 E955.508
G1 X-34.12 Y28.4 Z0.41 F3600.0 E955.528
G1 X34.12 Y28.4 Z0.41 F3600.0 E958.86
G1 X34.12 Y28.8 Z0.41 F3600.0 E958.88
G1 X-34.12 Y28.8 Z0.41 F3600.0 E962.213
G1 X-34.12 Y29.2 Z0.41 F3600.0 E962.232
G1 X34.12 Y29.2 Z0.41 F3600.0 E965.565
G1 X34.12 Y29.6 Z0.41 F3600.0 E965.585
G1 X-34.12 Y29.6 Z0.41 F3600.0 E968.918
M73 P54 (display progress)
G1 X-34.12 Y30.0 Z0.41 F3600.0 E968.937
G1 X34.12 Y30.0 Z0.41 F3600.0 E972.27
G1 X34.12 Y30.4 Z0.41 F3600.0 E972.289
G1 X-34.12 Y30.4 Z0.41 F3600.0 E975.622
G1 X-34.12 Y30.8 Z0.41 F3600.0 E975.642
G1 X34.12 Y30.8 Z0.41 F3600.0 E978.975
G1 X34.12 Y31.2 Z0.41 F3600.0 E978.994
G1 X-34.12 Y31.2 Z0.41 F3600.0 E982.327
G1 X-34.12 Y31.6 Z0.41 F3600.0 E982.346
G1 X34.12 Y31.6 Z0.41 F3600.0 E985.679
G1 X34.12 Y32.0 Z0.41 F3600.0 E985.699
G1 X-34.12 Y32.0 Z0.41 F3600.0 E989.032
G1 X-34.12 Y32.4 Z0.41 F3600.0 E989.051
G1 X34.12 Y32.4 Z0.41 F3600.0 E992.384
G1 X34.12 Y32.8 Z0.41 F3600.0 E992.404
G1 X-34.12 Y32.8 Z0.41 F3600.0 E995.736
G1 X-34.12 Y33.2 Z0.41 F3600.0 E995.756
G1 X34.12 Y33.2 Z0.41 F3600.0 E999.089
M73 P55 (display progress)
G1 X34.12 Y33.6 Z0.41 F3600.0 E999.108
G1 X-34.12 Y33.6 Z0.41 F3600.0 E1002.441
G1 X-34.12 Y34.0 Z0.41 F3600.0 E1002.461
G1 X34.12 Y34.0 Z0.41 F3600.0 E1005.793
G1 X34.12 Y34.4 Z0.41 F3600.0 E1005.813
G1 X-34.12 Y34.4 Z0.41 F3600.0 E1009.146
G1 X-34.12 Y34.8 Z0.41 F3600.0 E1009.165
G1 X34.12 Y34.8 Z0.41 F3600.0 E1012.498
G1 X34.12 Y35.2 Z0.41 F3600.0 E1012.518
G1 X-34.12 Y35.2 Z0.41 F3600.0 E1015.85
G1 X-34.12 Y35.6 Z0.41 F3600.0 E1015.87
G1 X34.12 Y35.6 Z0.41 F3600.0 E1019.203
G1 X34.12 Y36.0 Z0.41 F3600.0 E1019.222
G1 X-34.12 Y36.0 Z0.41 F3600.0 E1022.555
G1 X-34.12 Y36.4 Z0.41 F3600.0 E1022.575
G1 X34.12 Y36.4 Z0.41 F3600.0 E1025.908
G1 X34.12 Y36.8 Z0.41 F3600.0 E1025.927
G1 X-34.12 Y36.8 Z0.41 F3600.0 E1029.26
M73 P56 (display progress)
G1 X-34.12 Y37.2 Z0.41 F3600.0 E1029.279
G1 X34.12 Y37.2 Z0.41 F3600.0 E1032.612
G1 X34.12 Y37.6 Z0.41 F3600.0 E1032.632
G1 X-34.12 Y37.6 Z0.41 F3600.0 E1035.965
G1 X-34.12 Y38.0 Z0.41 F3600.0 E1035.984
G1 X34.12 Y38.0 Z0.41 F3600.0 E1039.317
G1 X34.12 Y38.4 Z0.41 F3600.0 E1039.336
G1 X-34.12 Y38.4 Z0.41 F3600.0 E1042.669
G1 X-34.12 Y38.8 Z0.41 F3600.0 E1042.689
G1 X34.12 Y38.8 Z0.41 F3600.0 E1046.022
G1 X34.12 Y39.2 Z0.41 F3600.0 E1046.041
G1 X-34.12 Y39.2 Z0.41 F3600.0 E1049.374
G1 X-34.12 Y39.6 Z0.41 F3600.0 E1049.393
G1 X34.12 Y39.6 Z0.41 F3600.0 E1052.726
G1 X34.12 Y40.0 Z0.41 F3600.0 E1052.746
G1 X-34.12 Y40.0 Z0.41 F3600.0 E1056.079
G1 X-34.12 Y40.4 Z0.41 F3600.0 E1056.098
G1 X34.12 Y40.4 Z0.41 F3600.0 E1059.431
M73 P57 (display progress)
G1 X34.12 Y40.8 Z0.41 F3600.0 E1059.451
G1 X-34.12 Y40.8 Z0.41 F3600.0 E1062.783
G1 X-34.12 Y41.2 Z0.41 F3600.0 E1062.803
G1 X34.12 Y41.2 Z0.41 F3600.0 E1066.136
G1 X34.12 Y41.6 Z0.41 F3600.0 E1066.155
G1 X-34.12 Y41.6 Z0.41 F3600.0 E1069.488
G1 X-34.12 Y42.0 Z0.41 F3600.0 E1069.508
G1 X34.12 Y42.0 Z0.41 F3600.0 E1072.84
G1 X34.12 Y42.4 Z0.41 F3600.0 E1072.86
G1 X-34.12 Y42.4 Z0.41 F3600.0 E1076.193
G1 X-34.12 Y42.8 Z0.41 F3600.0 E1076.212
G1 X34.12 Y42.8 Z0.41 F3600.0 E1079.545
G1 X34.12 Y43.2 Z0.41 F3600.0 E1079.565
G1 X-34.12 Y43.2 Z0.41 F3600.0 E1082.897
G1 X-34.12 Y43.6 Z0.41 F3600.0 E1082.917
G1 X34.12 Y43.6 Z0.41 F3600.0 E1086.25
G1 X34.01 Y44.0 Z0.41 F3600.0 E1086.27
G1 X-34.01 Y44.0 Z0.41 F3600.0 E1089.592
M73 P58 (display progress)
G1 F1200.0
G1 E1088.592
G1 F3600.0
M103
G1 X-1.2 Y1.2 Z0.41 F4500.0
G1 X-1.2 Y-1.2 Z0.41 F4500.0
G1 X34.01 Y-44.0 Z0.41 F4500.0
G1 F1200.0
G1 E1089.592
G1 F4500.0
M101
G1 X-34.01 Y-44.0 Z0.41 F3600.0 E1092.915
G1 X-34.12 Y-43.6 Z0.41 F3600.0 E1092.935
G1 X34.12 Y-43.6 Z0.41 F3600.0 E1096.268
G1 X34.12 Y-43.2 Z0.41 F3600.0 E1096.287
G1 X-34.12 Y-43.2 Z0.41 F3600.0 E1099.62
G1 X-34.12 Y-42.8 Z0.41 F3600.0 E1099.64
M73 P59 (display progress)
G1 X34.12 Y-42.8 Z0.41 F3600.0 E1102.972
G1 X34.12 Y-42.4 Z0.41 F3600.0 E1102.992
G1 X-34.12 Y-42.4 Z0.41 F3600.0 E1106.325
G1 X-34.12 Y-42.0 Z0.41 F3600.0 E1106.344
G1 X34.12 Y-42.0 Z0.41 F3600.0 E1109.677
G1 X34.12 Y-41.6 Z0.41 F3600.0 E1109.697
G1 X-34.12 Y-41.6 Z0.41 F3600.0 E1113.029
G1 X-34.12 Y-41.2 Z0.41 F3600.0 E1113.049
G1 X34.12 Y-41.2 Z0.41 F3600.0 E1116.382
G1 X34.12 Y-40.8 Z0.41 F3600.0 E1116.401
G1 X-34.12 Y-40.8 Z0.41 F3600.0 E1119.734
G1 X-34.12 Y-40.4 Z0.41 F3600.0 E1119.754
G1 X34.12 Y-40.4 Z0.41 F3600.0 E1123.086
G1 X34.12 Y-40.0 Z0.41 F3600.0 E1123.106
G1 X-34.12 Y-40.0 Z0.41 F3600.0 E1126.439
G1 X-34.12 Y-39.6 Z0.41 F3600.0 E1126.458
G1 X34.12 Y-39.6 Z0.41 F3600.0 E1129.791
G1 X34.12 Y-39.2 Z0.41 F3600.0 E1129.811
M73 P60 (display progress)
G1 X-34.12 Y-39.2 Z0.41 F3600.0 E1133.143
G1 X-34.12 Y-38.8 Z0.41 F3600.0 E1133.163
G1 X34.12 Y-38.8 Z0.41 F3600.0 E1136.496
G1 X34.12 Y-38.4 Z0.41 F3600.0 E1136.515
G1 X-34.12 Y-38.4 Z0.41 F3600.0 E1139.848
G1 X-34.12 Y-38.0 Z0.41 F3600.0 E1139.868
G1 X34.12 Y-38.0 Z0.41 F3600.0 E1143.201
G1 X34.12 Y-37.6 Z0.41 F3600.0 E1143.22
G1 X-34.12 Y-37.6 Z0.41 F3600.0 E1146.553
G1 X-34.12 Y-37.2 Z0.41 F3600.0 E1146.572
G1 X34.12 Y-37.2 Z0.41 F3600.0 E1149.905
G1 X34.12 Y-36.8 Z0.41 F3600.0 E1149.925
G1 X-34.12 Y-36.8 Z0.41 F3600.0 E1153.258
G1 X-34.12 Y-36.4 Z0.41 F3600.0 E1153.277
G1 X34.12 Y-36.4 Z0.41 F3600.0 E1156.61
G1 X34.12 Y-36.0 Z0.41 F3600.0 E1156.629
G1 X-34.12 Y-36.0 Z0.41 F3600.0 E1159.962
G1 X-34.12 Y-35.6 Z0.41 F3600.0 E1159.982
M73 P61 (display progress)
G1 X34.12 Y-35.6 Z0.41 F3600.0 E1163.315
G1 X34.12 Y-35.2 Z0.41 F3600.0 E1163.334
G1 X-34.12 Y-35.2 Z0.41 F3600.0 E1166.667
G1 X-34.12 Y-34.8 Z0.41 F3600.0 E1166.687
G1 X34.12 Y-34.8 Z0.41 F3600.0 E1170.019
G1 X34.12 Y-34.4 Z0.41 F3600.0 E1170.039
G1 X-34.12 Y-34.4 Z0.41 F3600.0 E1173.372
G1 X-34.12 Y-34.0 Z0.41 F3600.0 E1173.391
G1 X34.12 Y-34.0 Z0.41 F3600.0 E1176.724
G1 X34.12 Y-33.6 Z0.41 F3600.0 E1176.744
G1 X-34.12 Y-33.6 Z0.41 F3600.0 E1180.076
G1 X-34.12 Y-33.2 Z0.41 F3600.0 E1180.096
G1 X34.12 Y-33.2 Z0.41 F3600.0 E1183.429
G1 X34.12 Y-32.8 Z0.41 F3600.0 E1183.448
G1 X-34.12 Y-32.8 Z0.41 F3600.0 E1186.781
G1 X-34.12 Y-32.4 Z0.41 F3600.0 E1186.801
G1 X34.12 Y-32.4 Z0.41 F3600.0 E1190.133
G1 X34.12 Y-32.0 Z0.41 F3600.0 E1190.153
M73 P62 (display progress)
G1 X-34.12 Y-32.0 Z0.41 F3600.0 E1193.486
G1 X-34.12 Y-31.6 Z0.41 F3600.0 E1193.505
G1 X34.12 Y-31.6 Z0.41 F3600.0 E1196.838
G1 X34.12 Y-31.2 Z0.41 F3600.0 E1196.858
G1 X-34.12 Y-31.2 Z0.41 F3600.0 E1200.191
G1 X-34.12 Y-30.8 Z0.41 F3600.0 E1200.21
G1 X34.12 Y-30.8 Z0.41 F3600.0 E1203.543
G1 X34.12 Y-30.4 Z0.41 F3600.0 E1203.562
G1 X-34.12 Y-30.4 Z0.41 F3600.0 E1206.895
G1 X-34.12 Y-30.0 Z0.41 F3600.0 E1206.915
G1 X34.12 Y-30.0 Z0.41 F3600.0 E1210.248
G1 X34.12 Y-29.6 Z0.41 F3600.0 E1210.267
G1 X-34.12 Y-29.6 Z0.41 F3600.0 E1213.6
G1 X-34.12 Y-29.2 Z0.41 F3600.0 E1213.619
G1 X34.12 Y-29.2 Z0.41 F3600.0 E1216.952
G1 X34.12 Y-28.8 Z0.41 F3600.0 E1216.972
G1 X-34.12 Y-28.8 Z0.41 F3600.0 E1220.305
G1 X-34.12 Y-28.4 Z0.41 F3600.0 E1220.324
M73 P63 (display progress)
G1 X34.12 Y-28.4 Z0.41 F3600.0 E1223.657
G1 X34.12 Y-28.0 Z0.41 F3600.0 E1223.676
G1 X-34.12 Y-28.0 Z0.41 F3600.0 E1227.009
G1 X-34.12 Y-27.6 Z0.41 F3600.0 E1227.029
G1 X34.12 Y-27.6 Z0.41 F3600.0 E1230.362
G1 X34.12 Y-27.2 Z0.41 F3600.0 E1230.381
G1 X-34.12 Y-27.2 Z0.41 F3600.0 E1233.714
G1 X-34.12 Y-26.8 Z0.41 F3600.0 E1233.734
G1 X34.12 Y-26.8 Z0.41 F3600.0 E1237.066
G1 X34.12 Y-26.4 Z0.41 F3600.0 E1237.086
G1 X-34.12 Y-26.4 Z0.41 F3600.0 E1240.419
G1 X-34.12 Y-26.0 Z0.41 F3600.0 E1240.438
G1 X34.12 Y-26.0 Z0.41 F3600.0 E1243.771
G1 X34.12 Y-25.6 Z0.41 F3600.0 E1243.791
G1 X-34.12 Y-25.6 Z0.41 F3600.0 E1247.123
G1 X-34.12 Y-25.2 Z0.41 F3600.0 E1247.143
G1 X34.12 Y-25.2 Z0.41 F3600.0 E1250.476
G1 X34.12 Y-24.8 Z0.41 F3600.0 E1250.495
M73 P64 (display progress)
G1 X-34.12 Y-24.8 Z0.41 F3600.0 E1253.828
G1 X-34.12 Y-24.4 Z0.41 F3600.0 E1253.848
G1 X34.12 Y-24.4 Z0.41 F3600.0 E1257.18
G1 X34.12 Y-24.0 Z0.41 F3600.0 E1257.2
G1 X-34.12 Y-24.0 Z0.41 F3600.0 E1260.533
G1 X-34.12 Y-23.6 Z0.41 F3600.0 E1260.552
G1 X34.12 Y-23.6 Z0.41 F3600.0 E1263.885
G1 X34.12 Y-23.2 Z0.41 F3600.0 E1263.905
G1 X-34.12 Y-23.2 Z0.41 F3600.0 E1267.238
G1 X-34.12 Y-22.8 Z0.41 F3600.0 E1267.257
G1 X34.12 Y-22.8 Z0.41 F3600.0 E1270.59
G1 X34.12 Y-22.4 Z0.41 F3600.0 E1270.609
G1 X-34.12 Y-22.4 Z0.41 F3600.0 E1273.942
G1 X-34.12 Y-22.0 Z0.41 F3600.0 E1273.962
G1 X34.12 Y-22.0 Z0.41 F3600.0 E1277.295
G1 X34.12 Y-21.6 Z0.41 F3600.0 E1277.314
G1 X-34.12 Y-21.6 Z0.41 F3600.0 E1280.647
G1 X-34.12 Y-21.2 Z0.41 F3600.0 E1280.666
M73 P65 (display progress)
G1 X34.12 Y-21.2 Z0.41 F3600.0 E1283.999
G1 X34.12 Y-20.8 Z0.41 F3600.0 E1284.019
G1 X-34.12 Y-20.8 Z0.41 F3600.0 E1287.352
G1 X-34.12 Y-20.4 Z0.41 F3600.0 E1287.371
G1 X34.12 Y-20.4 Z0.41 F3600.0 E1290.704
G1 X34.12 Y-20.0 Z0.41 F3600.0 E1290.724
G1 X-34.12 Y-20.0 Z0.41 F3600.0 E1294.056
G1 X-34.12 Y-19.6 Z0.41 F3600.0 E1294.076
G1 X34.12 Y-19.6 Z0.41 F3600.0 E1297.409
G1 X34.12 Y-19.2 Z0.41 F3600.0 E1297.428
G1 X-34.12 Y-19.2 Z0.41 F3600.0 E1300.761
G1 X-34.12 Y-18.8 Z0.41 F3600.0 E1300.781
G1 X34.12 Y-18.8 Z0.41 F3600.0 E1304.113
G1 X34.12 Y-18.4 Z0.41 F3600.0 E1304.133
G1 X-34.12 Y-18.4 Z0.41 F3600.0 E1307.466
G1 X-34.12 Y-18.0 Z0.41 F3600.0 E1307.485
G1 X34.12 Y-18.0 Z0.41 F3600.0 E1310.818
G1 X34.12 Y-17.6 Z0.41 F3600.0 E1310.838
M73 P66 (display progress)
G1 X-34.12 Y-17.6 Z0.41 F3600.0 E1314.17
G1 X-34.12 Y-17.2 Z0.41 F3600.0 E1314.19
G1 X34.12 Y-17.2 Z0.41 F3600.0 E1317.523
G1 X34.12 Y-16.8 Z0.41 F3600.0 E1317.542
G1 X-34.12 Y-16.8 Z0.41 F3600.0 E1320.875
G1 X-34.12 Y-16.4 Z0.41 F3600.0 E1320.895
G1 X34.12 Y-16.4 Z0.41 F3600.0 E1324.227
G1 X34.12 Y-16.0 Z0.41 F3600.0 E1324.247
G1 X-34.12 Y-16.0 Z0.41 F3600.0 E1327.58
G1 X-34.12 Y-15.6 Z0.41 F3600.0 E1327.599
G1 X34.12 Y-15.6 Z0.41 F3600.0 E1330.932
G1 X34.12 Y-15.2 Z0.41 F3600.0 E1330.952
G1 X-34.12 Y-15.2 Z0.41 F3600.0 E1334.285
G1 X-34.12 Y-14.8 Z0.41 F3600.0 E1334.304
G1 X34.12 Y-14.8 Z0.41 F3600.0 E1337.637
G1 X34.12 Y-14.4 Z0.41 F3600.0 E1337.656
G1 X-34.12 Y-14.4 Z0.41 F3600.0 E1340.989
M73 P67 (display progress)
G1 X-34.12 Y-14.0 Z0.41 F3600.0 E1341.009
G1 X34.12 Y-14.0 Z0.41 F3600.0 E1344.342
G1 X34.12 Y-13.6 Z0.41 F3600.0 E1344.361
G1 X-34.12 Y-13.6 Z0.41 F3600.0 E1347.694
G1 X-34.12 Y-13.2 Z0.41 F3600.0 E1347.713
G1 X34.12 Y-13.2 Z0.41 F3600.0 E1351.046
G1 X34.12 Y-12.8 Z0.41 F3600.0 E1351.066
G1 X-34.12 Y-12.8 Z0.41 F3600.0 E1354.399
G1 X-34.12 Y-12.4 Z0.41 F3600.0 E1354.418
G1 X34.12 Y-12.4 Z0.41 F3600.0 E1357.751
G1 X34.12 Y-12.0 Z0.41 F3600.0 E1357.771
G1 X-34.12 Y-12.0 Z0.41 F3600.0 E1361.103
G1 X-34.12 Y-11.6 Z0.41 F3600.0 E1361.123
G1 X34.12 Y-11.6 Z0.41 F3600.0 E1364.456
G1 X34.12 Y-11.2 Z0.41 F3600.0 E1364.475
G1 X-34.12 Y-11.2 Z0.41 F3600.0 E1367.808
G1 X-34.12 Y-10.8 Z0.41 F3600.0 E1367.828
G1 X34.12 Y-10.8 Z0.41 F3600.0 E1371.16
M73 P68 (display progress)
G1 X34.12 Y-10.4 Z0.41 F3600.0 E1371.18
G1 X-34.12 Y-10.4 Z0.41 F3600.0 E1374.513
G1 X-34.12 Y-10.0 Z0.41 F3600.0 E1374.532
G1 X34.12 Y-10.0 Z0.41 F3600.0 E1377.865
G1 X34.12 Y-9.6 Z0.41 F3600.0 E1377.885
G1 X-34.12 Y-9.6 Z0.41 F3600.0 E1381.217
G1 X-34.12 Y-9.2 Z0.41 F3600.0 E1381.237
G1 X34.12 Y-9.2 Z0.41 F3600.0 E1384.57
G1 X34.12 Y-8.8 Z0.41 F3600.0 E1384.589
G1 X-34.12 Y-8.8 Z0.41 F3600.0 E1387.922
G1 X-34.12 Y-8.4 Z0.41 F3600.0 E1387.942
G1 X34.12 Y-8.4 Z0.41 F3600.0 E1391.274
G1 X34.12 Y-8.0 Z0.41 F3600.0 E1391.294
G1 X-34.12 Y-8.0 Z0.41 F3600.0 E1394.627
G1 X-34.12 Y-7.6 Z0.41 F3600.0 E1394.646
G1 X34.12 Y-7.6 Z0.41 F3600.0 E1397.979
G1 X34.12 Y-7.2 Z0.41 F3600.0 E1397.999
G1 X-34.12 Y-7.2 Z0.41 F3600.0 E1401.332
M73 P69 (display progress)
G1 X-34.12 Y-6.8 Z0.41 F3600.0 E1401.351
G1 X34.12 Y-6.8 Z0.41 F3600.0 E1404.684
G1 X34.12 Y-6.4 Z0.41 F3600.0 E1404.703
G1 X-34.12 Y-6.4 Z0.41 F3600.0 E1408.036
G1 X-34.12 Y-6.0 Z0.41 F3600.0 E1408.056
G1 X34.12 Y-6.0 Z0.41 F3600.0 E1411.389
G1 X34.12 Y-5.6 Z0.41 F3600.0 E1411.408
G1 X-34.12 Y-5.6 Z0.41 F3600.0 E1414.741
G1 X-34.12 Y-5.2 Z0.41 F3600.0 E1414.76
G1 X34.12 Y-5.2 Z0.41 F3600.0 E1418.093
G1 X34.12 Y-4.8 Z0.41 F3600.0 E1418.113
G1 X-34.12 Y-4.8 Z0.41 F3600.0 E1421.446
G1 X-34.12 Y-4.4 Z0.41 F3600.0 E1421.465
G1 X34.12 Y-4.4 Z0.41 F3600.0 E1424.798
G1 X34.12 Y-4.0 Z0.41 F3600.0 E1424.818
G1 X-34.12 Y-4.0 Z0.41 F3600.0 E1428.15
G1 X-34.12 Y-3.6 Z0.41 F3600.0 E1428.17
G1 X34.12 Y-3.6 Z0.41 F3600.0 E1431.503
M73 P70 (display progress)
G1 X34.12 Y-3.2 Z0.41 F3600.0 E1431.522
G1 X-34.12 Y-3.2 Z0.41 F3600.0 E1434.855
G1 X-34.12 Y-2.8 Z0.41 F3600.0 E1434.875
G1 X34.12 Y-2.8 Z0.41 F3600.0 E1438.207
G1 X34.12 Y-2.4 Z0.41 F3600.0 E1438.227
G1 X-34.12 Y-2.4 Z0.41 F3600.0 E1441.56
G1 X-34.12 Y-2.0 Z0.41 F3600.0 E1441.579
G1 X34.12 Y-2.0 Z0.41 F3600.0 E1444.912
G1 X34.12 Y-1.6 Z0.41 F3600.0 E1444.932
G1 X1.88 Y-1.6 Z0.41 F3600.0 E1446.506
G1 X1.88 Y-1.2 Z0.41 F3600.0 E1446.526
G1 X34.12 Y-1.2 Z0.41 F3600.0 E1448.1
G1 X34.12 Y-0.8 Z0.41 F3600.0 E1448.12
G1 X1.88 Y-0.8 Z0.41 F3600.0 E1449.694
G1 X1.88 Y-0.4 Z0.41 F3600.0 E1449.714
G1 X34.12 Y-0.4 Z0.41 F3600.0 E1451.289
G1 X34.12 Y-0.0 Z0.41 F3600.0 E1451.308
G1 X1.88 Y-0.0 Z0.41 F3600.0 E1452.883
M73 P71 (display progress)
G1 X1.88 Y0.4 Z0.41 F3600.0 E1452.902
G1 X34.12 Y0.4 Z0.41 F3600.0 E1454.477
G1 X34.12 Y0.8 Z0.41 F3600.0 E1454.496
G1 X1.88 Y0.8 Z0.41 F3600.0 E1456.071
G1 X1.88 Y1.2 Z0.41 F3600.0 E1456.091
G1 X34.12 Y1.2 Z0.41 F3600.0 E1457.665
G1 X34.12 Y1.6 Z0.41 F3600.0 E1457.685
G1 X1.88 Y1.6 Z0.41 F3600.0 E1459.259
G1 F1200.0
G1 E1458.259
G1 F3600.0
M103
(</infill>)
(</nestedRing>)
(</rotation>)
(</layer>)
(<operatingLayerEnd> </operatingLayerEnd>)
(<layer> 0.675 )
M73 P72 (display progress)
(<rotation> (6.12323399574e-17+1j) )
(<nestedRing>)
(<boundaryPerimeter>)
(<boundaryPoint> X35.0 Y-45.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X35.0 Y45.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y45.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X-35.0 Y-45.0 Z0.675 </boundaryPoint>)
(<loop> inner )
G1 X1.2 Y-1.2 Z0.68 F4500.0
G1 X-1.6 Y-1.42 Z0.68 F4500.0
G1 F1200.0
G1 E1459.259
G1 F4500.0
M101
G1 X-1.6 Y1.6 Z0.68 F3600.0 E1459.407
G1 X1.6 Y1.6 Z0.68 F3600.0 E1459.563
G1 X1.6 Y-1.6 Z0.68 F3600.0 E1459.719
G1 X-1.6 Y-1.6 Z0.68 F3600.0 E1459.875
M73 P73 (display progress)
G1 X-1.6 Y-1.42 Z0.68 F3600.0 E1459.884
G1 F1200.0
G1 E1458.884
G1 F3600.0
M103
(</loop>)
(<loop> outer )
G1 X-34.4 Y-1.78 Z0.68 F4500.0
G1 F1200.0
G1 E1459.884
G1 F4500.0
M101
G1 X-34.4 Y-44.4 Z0.68 F3600.0 E1461.966
G1 X34.4 Y-44.4 Z0.68 F3600.0 E1465.326
G1 X34.4 Y44.4 Z0.68 F3600.0 E1469.663
G1 X-34.4 Y44.4 Z0.68 F3600.0 E1473.023
G1 X-34.4 Y-1.6 Z0.68 F3600.0 E1475.27
G1 X-34.4 Y-1.78 Z0.68 F3600.0 E1475.279
M73 P74 (display progress)
G1 F1200.0
G1 E1475.279
G1 F3600.0
M103
(</loop>)
(<edge> outer )
;M108 S30.0
G1 X-34.8 Y-1.78 Z0.68 F4500.0
G1 F1200.0
G1 E1475.279
G1 F4500.0
M101
G1 X-34.8 Y-44.8 Z0.68 F1800.0 E1477.38
G1 X34.8 Y-44.8 Z0.68 F1800.0 E1480.779
G1 X34.8 Y44.8 Z0.68 F1800.0 E1485.155
G1 X-34.8 Y44.8 Z0.68 F1800.0 E1488.554
G1 X-34.8 Y-1.6 Z0.68 F1800.0 E1490.82
M73 P75 (display progress)
G1 X-34.8 Y-1.78 Z0.68 F1800.0 E1490.829
G1 F1200.0
G1 E1489.829
G1 F1800.0
M103
(</edge>)
(</boundaryPerimeter>)
(<nestedRing>)
(<boundaryPerimeter>)
(<boundaryPoint> X1.0 Y1.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X1.0 Y-1.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y-1.0 Z0.675 </boundaryPoint>)
(<boundaryPoint> X-1.0 Y1.0 Z0.675 </boundaryPoint>)
(<edge> inner )
G1 X-1.2 Y-1.02 Z0.68 F4500.0
G1 F1200.0
G1 E1490.829
G1 F4500.0
M73 P76 (display progress)
M101
G1 X-1.2 Y1.2 Z0.68 F1800.0 E1490.937
G1 X1.2 Y1.2 Z0.68 F1800.0 E1491.055
G1 X1.2 Y-1.2 Z0.68 F1800.0 E1491.172
G1 X-1.2 Y-1.2 Z0.68 F1800.0 E1491.289
G1 X-1.2 Y-1.02 Z0.68 F1800.0 E1491.298
G1 F1200.0
G1 E1491.298
G1 F1800.0
M103
(</edge>)
(</boundaryPerimeter>)
(</nestedRing>)
(<infill>)
(<infillBoundary>)
(<infillPoint> X34.4 Y-44.4 Z0.675 </infillPoint>)
(<infillPoint> X34.4 Y44.4 Z0.675 </infillPoint>)
(<infillPoint> X-34.4 Y44.4 Z0.675 </infillPoint>)
M73 P77 (display progress)
(<infillPoint> X-34.4 Y-44.4 Z0.675 </infillPoint>)
(</infillBoundary>)
(<infillBoundary>)
(<infillPoint> X1.6 Y1.6 Z0.675 </infillPoint>)
(<infillPoint> X1.6 Y-1.6 Z0.675 </infillPoint>)
(<infillPoint> X-1.6 Y-1.6 Z0.675 </infillPoint>)
(<infillPoint> X-1.6 Y1.6 Z0.675 </infillPoint>)
(</infillBoundary>)
;M108 S60.0
G1 X-1.6 Y-1.88 Z0.68 F4500.0
G1 F1200.0
G1 E1491.298
G1 F4500.0
M101
G1 X-1.6 Y-44.12 Z0.68 F3600.0 E1493.361
G1 X-1.2 Y-44.12 Z0.68 F3600.0 E1493.381
G1 X-1.2 Y-1.88 Z0.68 F3600.0 E1495.444
G1 X-0.8 Y-1.88 Z0.68 F3600.0 E1495.463
M73 P78 (display progress)
G1 X-0.8 Y-44.12 Z0.68 F3600.0 E1497.526
G1 X-0.4 Y-44.12 Z0.68 F3600.0 E1497.546
G1 X-0.4 Y-1.88 Z0.68 F3600.0 E1499.609
G1 X-0.0 Y-1.88 Z0.68 F3600.0 E1499.628
G1 X-0.0 Y-44.12 Z0.68 F3600.0 E1501.691
G1 X0.4 Y-44.12 Z0.68 F3600.0 E1501.711
G1 X0.4 Y-1.88 Z0.68 F3600.0 E1503.774
G1 X0.8 Y-1.88 Z0.68 F3600.0 E1503.793
G1 X0.8 Y-44.12 Z0.68 F3600.0 E1505.856
G1 X1.2 Y-44.12 Z0.68 F3600.0 E1505.876
G1 X1.2 Y-1.88 Z0.68 F3600.0 E1507.939
G1 X1.6 Y-1.88 Z0.68 F3600.0 E1507.958
G1 X1.6 Y-44.12 Z0.68 F3600.0 E1510.021
G1 X2.0 Y-44.12 Z0.68 F3600.0 E1510.041
G1 X2.0 Y44.12 Z0.68 F3600.0 E1514.35
G1 X2.4 Y44.12 Z0.68 F3600.0 E1514.37
G1 X2.4 Y-44.12 Z0.68 F3600.0 E1518.679
G1 X2.8 Y-44.12 Z0.68 F3600.0 E1518.699
M73 P79 (display progress)
G1 X2.8 Y44.12 Z0.68 F3600.0 E1523.009
G1 X3.2 Y44.12 Z0.68 F3600.0 E1523.028
G1 X3.2 Y-44.12 Z0.68 F3600.0 E1527.338
G1 X3.6 Y-44.12 Z0.68 F3600.0 E1527.357
G1 X3.6 Y44.12 Z0.68 F3600.0 E1531.667
G1 X4.0 Y44.12 Z0.68 F3600.0 E1531.686
G1 X4.0 Y-44.12 Z0.68 F3600.0 E1535.996
G1 X4.4 Y-44.12 Z0.68 F3600.0 E1536.016
G1 X4.4 Y44.12 Z0.68 F3600.0 E1540.325
G1 X4.8 Y44.12 Z0.68 F3600.0 E1540.345
G1 X4.8 Y-44.12 Z0.68 F3600.0 E1544.654
G1 X5.2 Y-44.12 Z0.68 F3600.0 E1544.674
G1 X5.2 Y44.12 Z0.68 F3600.0 E1548.983
G1 X5.6 Y44.12 Z0.68 F3600.0 E1549.003
G1 X5.6 Y-44.12 Z0.68 F3600.0 E1553.313
G1 X6.0 Y-44.12 Z0.68 F3600.0 E1553.332
G1 X6.0 Y44.12 Z0.68 F3600.0 E1557.642
G1 X6.4 Y44.12 Z0.68 F3600.0 E1557.661
M73 P80 (display progress)
G1 X6.4 Y-44.12 Z0.68 F3600.0 E1561.971
G1 X6.8 Y-44.12 Z0.68 F3600.0 E1561.99
G1 X6.8 Y44.12 Z0.68 F3600.0 E1566.3
G1 X7.2 Y44.12 Z0.68 F3600.0 E1566.32
G1 X7.2 Y-44.12 Z0.68 F3600.0 E1570.629
G1 X7.6 Y-44.12 Z0.68 F3600.0 E1570.649
G1 X7.6 Y44.12 Z0.68 F3600.0 E1574.958
G1 X8.0 Y44.12 Z0.68 F3600.0 E1574.978
G1 X8.0 Y-44.12 Z0.68 F3600.0 E1579.287
G1 X8.4 Y-44.12 Z0.68 F3600.0 E1579.307
G1 X8.4 Y44.12 Z0.68 F3600.0 E1583.617
G1 X8.8 Y44.12 Z0.68 F3600.0 E1583.636
G1 X8.8 Y-44.12 Z0.68 F3600.0 E1587.946
G1 X9.2 Y-44.12 Z0.68 F3600.0 E1587.965
G1 X9.2 Y44.12 Z0.68 F3600.0 E1592.275
G1 X9.6 Y44.12 Z0.68 F3600.0 E1592.294
G1 X9.6 Y-44.12 Z0.68 F3600.0 E1596.604
G1 X10.0 Y-44.12 Z0.68 F3600.0 E1596.624
M73 P81 (display progress)
G1 X10.0 Y44.12 Z0.68 F3600.0 E1600.933
G1 X10.4 Y44.12 Z0.68 F3600.0 E1600.953
G1 X10.4 Y-44.12 Z0.68 F3600.0 E1605.262
G1 X10.8 Y-44.12 Z0.68 F3600.0 E1605.282
G1 X10.8 Y44.12 Z0.68 F3600.0 E1609.591
G1 X11.2 Y44.12 Z0.68 F3600.0 E1609.611
G1 X11.2 Y-44.12 Z0.68 F3600.0 E1613.921
G1 X11.6 Y-44.12 Z0.68 F3600.0 E1613.94
G1 X11.6 Y44.12 Z0.68 F3600.0 E1618.25
G1 X12.0 Y44.12 Z0.68 F3600.0 E1618.269
G1 X12.0 Y-44.12 Z0.68 F3600.0 E1622.579
G1 X12.4 Y-44.12 Z0.68 F3600.0 E1622.598
G1 X12.4 Y44.12 Z0.68 F3600.0 E1626.908
G1 X12.8 Y44.12 Z0.68 F3600.0 E1626.928
G1 X12.8 Y-44.12 Z0.68 F3600.0 E1631.237
G1 X13.2 Y-44.12 Z0.68 F3600.0 E1631.257
G1 X13.2 Y44.12 Z0.68 F3600.0 E1635.566
G1 X13.6 Y44.12 Z0.68 F3600.0 E1635.586
M73 P82 (display progress)
G1 X13.6 Y-44.12 Z0.68 F3600.0 E1639.895
G1 X14.0 Y-44.12 Z0.68 F3600.0 E1639.915
G1 X14.0 Y44.12 Z0.68 F3600.0 E1644.225
G1 X14.4 Y44.12 Z0.68 F3600.0 E1644.244
G1 X14.4 Y-44.12 Z0.68 F3600.0 E1648.554
G1 X14.8 Y-44.12 Z0.68 F3600.0 E1648.573
G1 X14.8 Y44.12 Z0.68 F3600.0 E1652.883
G1 X15.2 Y44.12 Z0.68 F3600.0 E1652.902
G1 X15.2 Y-44.12 Z0.68 F3600.0 E1657.212
G1 X15.6 Y-44.12 Z0.68 F3600.0 E1657.232
G1 X15.6 Y44.12 Z0.68 F3600.0 E1661.541
G1 X16.0 Y44.12 Z0.68 F3600.0 E1661.561
G1 X16.0 Y-44.12 Z0.68 F3600.0 E1665.87
G1 X16.4 Y-44.12 Z0.68 F3600.0 E1665.89
G1 X16.4 Y44.12 Z0.68 F3600.0 E1670.199
G1 X16.8 Y44.12 Z0.68 F3600.0 E1670.219
G1 X16.8 Y-44.12 Z0.68 F3600.0 E1674.529
G1 X17.2 Y-44.12 Z0.68 F3600.0 E1674.548
M73 P83 (display progress)
G1 X17.2 Y44.12 Z0.68 F3600.0 E1678.858
G1 X17.6 Y44.12 Z0.68 F3600.0 E1678.877
G1 X17.6 Y-44.12 Z0.68 F3600.0 E1683.187
G1 X18.0 Y-44.12 Z0.68 F3600.0 E1683.206
G1 X18.0 Y44.12 Z0.68 F3600.0 E1687.516
G1 X18.4 Y44.12 Z0.68 F3600.0 E1687.536
G1 X18.4 Y-44.12 Z0.68 F3600.0 E1691.845
G1 X18.8 Y-44.12 Z0.68 F3600.0 E1691.865
G1 X18.8 Y44.12 Z0.68 F3600.0 E1696.174
G1 X19.2 Y44.12 Z0.68 F3600.0 E1696.194
G1 X19.2 Y-44.12 Z0.68 F3600.0 E1700.503
G1 X19.6 Y-44.12 Z0.68 F3600.0 E1700.523
G1 X19.6 Y44.12 Z0.68 F3600.0 E1704.833
G1 X20.0 Y44.12 Z0.68 F3600.0 E1704.852
G1 X20.0 Y-44.12 Z0.68 F3600.0 E1709.162
G1 X20.4 Y-44.12 Z0.68 F3600.0 E1709.181
G1 X20.4 Y44.12 Z0.68 F3600.0 E1713.491
M73 P84 (display progress)
G1 X20.8 Y44.12 Z0.68 F3600.0 E1713.51
G1 X20.8 Y-44.12 Z0.68 F3600.0 E1717.82
G1 X21.2 Y-44.12 Z0.68 F3600.0 E1717.84
G1 X21.2 Y44.12 Z0.68 F3600.0 E1722.149
G1 X21.6 Y44.12 Z0.68 F3600.0 E1722.169
G1 X21.6 Y-44.12 Z0.68 F3600.0 E1726.478
G1 X22.0 Y-44.12 Z0.68 F3600.0 E1726.498
G1 X22.0 Y44.12 Z0.68 F3600.0 E1730.807
G1 X22.4 Y44.12 Z0.68 F3600.0 E1730.827
G1 X22.4 Y-44.12 Z0.68 F3600.0 E1735.137
G1 X22.8 Y-44.12 Z0.68 F3600.0 E1735.156
G1 X22.8 Y44.12 Z0.68 F3600.0 E1739.466
G1 X23.2 Y44.12 Z0.68 F3600.0 E1739.485
G1 X23.2 Y-44.12 Z0.68 F3600.0 E1743.795
G1 X23.6 Y-44.12 Z0.68 F3600.0 E1743.814
G1 X23.6 Y44.12 Z0.68 F3600.0 E1748.124
G1 X24.0 Y44.12 Z0.68 F3600.0 E1748.144
G1 X24.0 Y-44.12 Z0.68 F3600.0 E1752.453
M73 P85 (display progress)
G1 X24.4 Y-44.12 Z0.68 F3600.0 E1752.473
G1 X24.4 Y44.12 Z0.68 F3600.0 E1756.782
G1 X24.8 Y44.12 Z0.68 F3600.0 E1756.802
G1 X24.8 Y-44.12 Z0.68 F3600.0 E1761.111
G1 X25.2 Y-44.12 Z0.68 F3600.0 E1761.131
G1 X25.2 Y44.12 Z0.68 F3600.0 E1765.441
G1 X25.6 Y44.12 Z0.68 F3600.0 E1765.46
G1 X25.6 Y-44.12 Z0.68 F3600.0 E1769.77
G1 X26.0 Y-44.12 Z0.68 F3600.0 E1769.789
G1 X26.0 Y44.12 Z0.68 F3600.0 E1774.099
G1 X26.4 Y44.12 Z0.68 F3600.0 E1774.118
G1 X26.4 Y-44.12 Z0.68 F3600.0 E1778.428
G1 X26.8 Y-44.12 Z0.68 F3600.0 E1778.448
G1 X26.8 Y44.12 Z0.68 F3600.0 E1782.757
G1 X27.2 Y44.12 Z0.68 F3600.0 E1782.777
G1 X27.2 Y-44.12 Z0.68 F3600.0 E1787.086
G1 X27.6 Y-44.12 Z0.68 F3600.0 E1787.106
G1 X27.6 Y44.12 Z0.68 F3600.0 E1791.415
M73 P86 (display progress)
G1 X28.0 Y44.12 Z0.68 F3600.0 E1791.435
G1 X28.0 Y-44.12 Z0.68 F3600.0 E1795.745
G1 X28.4 Y-44.12 Z0.68 F3600.0 E1795.764
G1 X28.4 Y44.12 Z0.68 F3600.0 E1800.074
G1 X28.8 Y44.12 Z0.68 F3600.0 E1800.093
G1 X28.8 Y-44.12 Z0.68 F3600.0 E1804.403
G1 X29.2 Y-44.12 Z0.68 F3600.0 E1804.422
G1 X29.2 Y44.12 Z0.68 F3600.0 E1808.732
G1 X29.6 Y44.12 Z0.68 F3600.0 E1808.752
G1 X29.6 Y-44.12 Z0.68 F3600.0 E1813.061
G1 X30.0 Y-44.12 Z0.68 F3600.0 E1813.081
G1 X30.0 Y44.12 Z0.68 F3600.0 E1817.39
G1 X30.4 Y44.12 Z0.68 F3600.0 E1817.41
G1 X30.4 Y-44.12 Z0.68 F3600.0 E1821.719
G1 X30.8 Y-44.12 Z0.68 F3600.0 E1821.739
G1 X30.8 Y44.12 Z0.68 F3600.0 E1826.049
G1 X31.2 Y44.12 Z0.68 F3600.0 E1826.068
G1 X31.2 Y-44.12 Z0.68 F3600.0 E1830.378
M73 P87 (display progress)
G1 X31.6 Y-44.12 Z0.68 F3600.0 E1830.397
G1 X31.6 Y44.12 Z0.68 F3600.0 E1834.707
G1 X32.0 Y44.12 Z0.68 F3600.0 E1834.726
G1 X32.0 Y-44.12 Z0.68 F3600.0 E1839.036
G1 X32.4 Y-44.12 Z0.68 F3600.0 E1839.056
G1 X32.4 Y44.12 Z0.68 F3600.0 E1843.365
G1 X32.8 Y44.12 Z0.68 F3600.0 E1843.385
G1 X32.8 Y-44.12 Z0.68 F3600.0 E1847.694
G1 X33.2 Y-44.12 Z0.68 F3600.0 E1847.714
G1 X33.2 Y44.12 Z0.68 F3600.0 E1852.023
G1 X33.6 Y44.12 Z0.68 F3600.0 E1852.043
G1 X33.6 Y-44.12 Z0.68 F3600.0 E1856.353
G1 X34.0 Y-44.01 Z0.68 F3600.0 E1856.373
G1 X34.0 Y44.01 Z0.68 F3600.0 E1860.672
G1 F1200.0
G1 E1859.672
G1 F3600.0
M103
M73 P88 (display progress)
G1 X-1.6 Y1.88 Z0.68 F4500.0
G1 F1200.0
G1 E1860.672
G1 F4500.0
M101
G1 X-1.6 Y44.12 Z0.68 F3600.0 E1862.735
G1 X-1.2 Y44.12 Z0.68 F3600.0 E1862.754
G1 X-1.2 Y1.88 Z0.68 F3600.0 E1864.817
G1 X-0.8 Y1.88 Z0.68 F3600.0 E1864.837
G1 X-0.8 Y44.12 Z0.68 F3600.0 E1866.9
G1 X-0.4 Y44.12 Z0.68 F3600.0 E1866.919
G1 X-0.4 Y1.88 Z0.68 F3600.0 E1868.982
G1 X0.0 Y1.88 Z0.68 F3600.0 E1869.002
G1 X0.0 Y44.12 Z0.68 F3600.0 E1871.065
G1 X0.4 Y44.12 Z0.68 F3600.0 E1871.084
G1 X0.4 Y1.88 Z0.68 F3600.0 E1873.147
G1 X0.8 Y1.88 Z0.68 F3600.0 E1873.167
G1 X0.8 Y44.12 Z0.68 F3600.0 E1875.23
M73 P89 (display progress)
G1 X1.2 Y44.12 Z0.68 F3600.0 E1875.25
G1 X1.2 Y1.88 Z0.68 F3600.0 E1877.313
G1 X1.6 Y1.88 Z0.68 F3600.0 E1877.332
G1 X1.6 Y44.12 Z0.68 F3600.0 E1879.395
G1 F1200.0
G1 E1878.395
G1 F3600.0
M103
G1 X-2.0 Y44.12 Z0.68 F4500.0
G1 F1200.0
G1 E1879.395
G1 F4500.0
M101
G1 X-2.0 Y-44.12 Z0.68 F3600.0 E1883.705
G1 X-2.4 Y-44.12 Z0.68 F3600.0 E1883.724
G1 X-2.4 Y44.12 Z0.68 F3600.0 E1888.034
G1 X-2.8 Y44.12 Z0.68 F3600.0 E1888.053
G1 X-2.8 Y-44.12 Z0.68 F3600.0 E1892.363
M73 P90 (display progress)
G1 X-3.2 Y-44.12 Z0.68 F3600.0 E1892.382
G1 X-3.2 Y44.12 Z0.68 F3600.0 E1896.692
G1 X-3.6 Y44.12 Z0.68 F3600.0 E1896.712
G1 X-3.6 Y-44.12 Z0.68 F3600.0 E1901.021
G1 X-4.0 Y-44.12 Z0.68 F3600.0 E1901.041
G1 X-4.0 Y44.12 Z0.68 F3600.0 E1905.35
G1 X-4.4 Y44.12 Z0.68 F3600.0 E1905.37
G1 X-4.4 Y-44.12 Z0.68 F3600.0 E1909.68
G1 X-4.8 Y-44.12 Z0.68 F3600.0 E1909.699
G1 X-4.8 Y44.12 Z0.68 F3600.0 E1914.009
G1 X-5.2 Y44.12 Z0.68 F3600.0 E1914.028
G1 X-5.2 Y-44.12 Z0.68 F3600.0 E1918.338
G1 X-5.6 Y-44.12 Z0.68 F3600.0 E1918.357
G1 X-5.6 Y44.12 Z0.68 F3600.0 E1922.667
G1 X-6.0 Y44.12 Z0.68 F3600.0 E1922.686
G1 X-6.0 Y-44.12 Z0.68 F3600.0 E1926.996
G1 X-6.4 Y-44.12 Z0.68 F3600.0 E1927.016
G1 X-6.4 Y44.12 Z0.68 F3600.0 E1931.325
M73 P91 (display progress)
G1 X-6.8 Y44.12 Z0.68 F3600.0 E1931.345
G1 X-6.8 Y-44.12 Z0.68 F3600.0 E1935.654
G1 X-7.2 Y-44.12 Z0.68 F3600.0 E1935.674
G1 X-7.2 Y44.12 Z0.68 F3600.0 E1939.984
G1 X-7.6 Y44.12 Z0.68 F3600.0 E1940.003
G1 X-7.6 Y-44.12 Z0.68 F3600.0 E1944.313
G1 X-8.0 Y-44.12 Z0.68 F3600.0 E1944.332
G1 X-8.0 Y44.12 Z0.68 F3600.0 E1948.642
G1 X-8.4 Y44.12 Z0.68 F3600.0 E1948.661
G1 X-8.4 Y-44.12 Z0.68 F3600.0 E1952.971
G1 X-8.8 Y-44.12 Z0.68 F3600.0 E1952.99
G1 X-8.8 Y44.12 Z0.68 F3600.0 E1957.3
G1 X-9.2 Y44.12 Z0.68 F3600.0 E1957.32
G1 X-9.2 Y-44.12 Z0.68 F3600.0 E1961.629
G1 X-9.6 Y-44.12 Z0.68 F3600.0 E1961.649
G1 X-9.6 Y44.12 Z0.68 F3600.0 E1965.958
G1 X-10.0 Y44.12 Z0.68 F3600.0 E1965.978
M73 P92 (display progress)
G1 X-10.0 Y-44.12 Z0.68 F3600.0 E1970.288
G1 X-10.4 Y-44.12 Z0.68 F3600.0 E1970.307
G1 X-10.4 Y44.12 Z0.68 F3600.0 E1974.617
G1 X-10.8 Y44.12 Z0.68 F3600.0 E1974.636
G1 X-10.8 Y-44.12 Z0.68 F3600.0 E1978.946
G1 X-11.2 Y-44.12 Z0.68 F3600.0 E1978.965
G1 X-11.2 Y44.12 Z0.68 F3600.0 E1983.275
G1 X-11.6 Y44.12 Z0.68 F3600.0 E1983.294
G1 X-11.6 Y-44.12 Z0.68 F3600.0 E1987.604
G1 X-12.0 Y-44.12 Z0.68 F3600.0 E1987.624
G1 X-12.0 Y44.12 Z0.68 F3600.0 E1991.933
G1 X-12.4 Y44.12 Z0.68 F3600.0 E1991.953
G1 X-12.4 Y-44.12 Z0.68 F3600.0 E1996.262
G1 X-12.8 Y-44.12 Z0.68 F3600.0 E1996.282
G1 X-12.8 Y44.12 Z0.68 F3600.0 E2000.592
G1 X-13.2 Y44.12 Z0.68 F3600.0 E2000.611
G1 X-13.2 Y-44.12 Z0.68 F3600.0 E2004.921
G1 X-13.6 Y-44.12 Z0.68 F3600.0 E2004.94
M73 P93 (display progress)
G1 X-13.6 Y44.12 Z0.68 F3600.0 E2009.25
G1 X-14.0 Y44.12 Z0.68 F3600.0 E2009.269
G1 X-14.0 Y-44.12 Z0.68 F3600.0 E2013.579
G1 X-14.4 Y-44.12 Z0.68 F3600.0 E2013.598
G1 X-14.4 Y44.12 Z0.68 F3600.0 E2017.908
G1 X-14.8 Y44.12 Z0.68 F3600.0 E2017.928
G1 X-14.8 Y-44.12 Z0.68 F3600.0 E2022.237
G1 X-15.2 Y-44.12 Z0.68 F3600.0 E2022.257
G1 X-15.2 Y44.12 Z0.68 F3600.0 E2026.566
G1 X-15.6 Y44.12 Z0.68 F3600.0 E2026.586
G1 X-15.6 Y-44.12 Z0.68 F3600.0 E2030.896
G1 X-16.0 Y-44.12 Z0.68 F3600.0 E2030.915
G1 X-16.0 Y44.12 Z0.68 F3600.0 E2035.225
G1 X-16.4 Y44.12 Z0.68 F3600.0 E2035.244
G1 X-16.4 Y-44.12 Z0.68 F3600.0 E2039.554
G1 X-16.8 Y-44.12 Z0.68 F3600.0 E2039.573
G1 X-16.8 Y44.12 Z0.68 F3600.0 E2043.883
G1 X-17.2 Y44.12 Z0.68 F3600.0 E2043.902
M73 P94 (display progress)
G1 X-17.2 Y-44.12 Z0.68 F3600.0 E2048.212
G1 X-17.6 Y-44.12 Z0.68 F3600.0 E2048.232
G1 X-17.6 Y44.12 Z0.68 F3600.0 E2052.541
G1 X-18.0 Y44.12 Z0.68 F3600.0 E2052.561
G1 X-18.0 Y-44.12 Z0.68 F3600.0 E2056.87
G1 X-18.4 Y-44.12 Z0.68 F3600.0 E2056.89
G1 X-18.4 Y44.12 Z0.68 F3600.0 E2061.2
G1 X-18.8 Y44.12 Z0.68 F3600.0 E2061.219
G1 X-18.8 Y-44.12 Z0.68 F3600.0 E2065.529
G1 X-19.2 Y-44.12 Z0.68 F3600.0 E2065.548
G1 X-19.2 Y44.12 Z0.68 F3600.0 E2069.858
G1 X-19.6 Y44.12 Z0.68 F3600.0 E2069.877
G1 X-19.6 Y-44.12 Z0.68 F3600.0 E2074.187
G1 X-20.0 Y-44.12 Z0.68 F3600.0 E2074.206
G1 X-20.0 Y44.12 Z0.68 F3600.0 E2078.516
G1 X-20.4 Y44.12 Z0.68 F3600.0 E2078.536
G1 X-20.4 Y-44.12 Z0.68 F3600.0 E2082.845
G1 X-20.8 Y-44.12 Z0.68 F3600.0 E2082.865
M73 P95 (display progress)
G1 X-20.8 Y44.12 Z0.68 F3600.0 E2087.174
G1 X-21.2 Y44.12 Z0.68 F3600.0 E2087.194
G1 X-21.2 Y-44.12 Z0.68 F3600.0 E2091.504
G1 X-21.6 Y-44.12 Z0.68 F3600.0 E2091.523
G1 X-21.6 Y44.12 Z0.68 F3600.0 E2095.833
G1 X-22.0 Y44.12 Z0.68 F3600.0 E2095.852
G1 X-22.0 Y-44.12 Z0.68 F3600.0 E2100.162
G1 X-22.4 Y-44.12 Z0.68 F3600.0 E2100.181
G1 X-22.4 Y44.12 Z0.68 F3600.0 E2104.491
G1 X-22.8 Y44.12 Z0.68 F3600.0 E2104.51
G1 X-22.8 Y-44.12 Z0.68 F3600.0 E2108.82
G1 X-23.2 Y-44.12 Z0.68 F3600.0 E2108.84
G1 X-23.2 Y44.12 Z0.68 F3600.0 E2113.149
G1 X-23.6 Y44.12 Z0.68 F3600.0 E2113.169
G1 X-23.6 Y-44.12 Z0.68 F3600.0 E2117.478
G1 X-24.0 Y-44.12 Z0.68 F3600.0 E2117.498
G1 X-24.0 Y44.12 Z0.68 F3600.0 E2121.808
G1 X-24.4 Y44.12 Z0.68 F3600.0 E2121.827
M73 P96 (display progress)
G1 X-24.4 Y-44.12 Z0.68 F3600.0 E2126.137
G1 X-24.8 Y-44.12 Z0.68 F3600.0 E2126.156
G1 X-24.8 Y44.12 Z0.68 F3600.0 E2130.466
G1 X-25.2 Y44.12 Z0.68 F3600.0 E2130.485
G1 X-25.2 Y-44.12 Z0.68 F3600.0 E2134.795
G1 X-25.6 Y-44.12 Z0.68 F3600.0 E2134.814
G1 X-25.6 Y44.12 Z0.68 F3600.0 E2139.124
G1 X-26.0 Y44.12 Z0.68 F3600.0 E2139.144
G1 X-26.0 Y-44.12 Z0.68 F3600.0 E2143.453
G1 X-26.4 Y-44.12 Z0.68 F3600.0 E2143.473
G1 X-26.4 Y44.12 Z0.68 F3600.0 E2147.782
G1 X-26.8 Y44.12 Z0.68 F3600.0 E2147.802
G1 X-26.8 Y-44.12 Z0.68 F3600.0 E2152.112
G1 X-27.2 Y-44.12 Z0.68 F3600.0 E2152.131
G1 X-27.2 Y44.12 Z0.68 F3600.0 E2156.441
G1 X-27.6 Y44.12 Z0.68 F3600.0 E2156.46
G1 X-27.6 Y-44.12 Z0.68 F3600.0 E2160.77
G1 X-28.0 Y-44.12 Z0.68 F3600.0 E2160.789
M73 P97 (display progress)
G1 X-28.0 Y44.12 Z0.68 F3600.0 E2165.099
G1 X-28.4 Y44.12 Z0.68 F3600.0 E2165.118
G1 X-28.4 Y-44.12 Z0.68 F3600.0 E2169.428
G1 X-28.8 Y-44.12 Z0.68 F3600.0 E2169.448
G1 X-28.8 Y44.12 Z0.68 F3600.0 E2173.757
G1 X-29.2 Y44.12 Z0.68 F3600.0 E2173.777
G1 X-29.2 Y-44.12 Z0.68 F3600.0 E2178.086
G1 X-29.6 Y-44.12 Z0.68 F3600.0 E2178.106
G1 X-29.6 Y44.12 Z0.68 F3600.0 E2182.416
G1 X-30.0 Y44.12 Z0.68 F3600.0 E2182.435
G1 X-30.0 Y-44.12 Z0.68 F3600.0 E2186.745
G1 X-30.4 Y-44.12 Z0.68 F3600.0 E2186.764
G1 X-30.4 Y44.12 Z0.68 F3600.0 E2191.074
G1 X-30.8 Y44.12 Z0.68 F3600.0 E2191.093
G1 X-30.8 Y-44.12 Z0.68 F3600.0 E2195.403
G1 X-31.2 Y-44.12 Z0.68 F3600.0 E2195.422
G1 X-31.2 Y44.12 Z0.68 F3600.0 E2199.732
G1 X-31.6 Y44.12 Z0.68 F3600.0 E2199.752
M73 P98 (display progress)
G1 X-31.6 Y-44.12 Z0.68 F3600.0 E2204.061
G1 X-32.0 Y-44.12 Z0.68 F3600.0 E2204.081
G1 X-32.0 Y44.12 Z0.68 F3600.0 E2208.39
G1 X-32.4 Y44.12 Z0.68 F3600.0 E2208.41
G1 X-32.4 Y-44.12 Z0.68 F3600.0 E2212.72
G1 X-32.8 Y-44.12 Z0.68 F3600.0 E2212.739
G1 X-32.8 Y44.12 Z0.68 F3600.0 E2217.049
G1 X-33.2 Y44.12 Z0.68 F3600.0 E2217.068
G1 X-33.2 Y-44.12 Z0.68 F3600.0 E2221.378
G1 X-33.6 Y-44.12 Z0.68 F3600.0 E2221.397
G1 X-33.6 Y44.12 Z0.68 F3600.0 E2225.707
G1 X-34.0 Y44.01 Z0.68 F3600.0 E2225.727
G1 X-34.0 Y-44.01 Z0.68 F3600.0 E2230.026
G1 F1200.0
G1 E2229.026
G1 F3600.0
M103
(</infill>)
M73 P99 (display progress)
(</nestedRing>)
(</rotation>)
(</layer>)
(</crafting>)
