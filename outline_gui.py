from gcode_outline import gcode_looper
import tkinter as tk
from tkinter.filedialog import asksaveasfilename

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

        self.looper = gcode_looper()
        self.looper.set_section('header', open('header.gcode'))
        self.looper.set_section('footer', open('footer.gcode'))
        self.looper.set_section('base', open('boring_base.gcode'))


    def createWidgets(self):
        self.input = tk.Text(width=1,height=1,font="normal 50")
        self.output = tk.Text(width=1,height=1)
        self.send = tk.Button(text="Save",command=self.send)

        self.input.pack(side='left',expand=1,fill='both')
        self.send.pack(side='bottom',fill='both')
        self.output.pack(side='left',expand=1,fill='both')

    def send(self):
        code = self.input.get(1.0,tk.END)
        filename = asksaveasfilename(filetypes=[("G-code files","*.gcode")])

        outfile = open(filename, 'w')
        self.looper.write(code, outfile, 15)
        outfile.close()

        readout = open(filename, 'r')

        self.output.delete(1.0,tk.END)
        self.output.insert(1.0,readout.read())

root = tk.Tk()
app = Application(master=root)
app.mainloop()
